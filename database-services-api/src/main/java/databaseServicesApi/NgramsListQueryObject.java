package databaseServicesApi;

public class NgramsListQueryObject {
    public static class NgramsListQueryBuilder {
        //required
        private int amount;
        private int offsetPage;

        //optional
        private String orderByFieldNameString;
        private NgramsSortFieldName orderByFieldName;
        private String orderString;
        private Order order;

        public NgramsListQueryBuilder(int amount, int offsetPage) {
            this.amount = amount;
            this.offsetPage = offsetPage;
        }

        public NgramsListQueryBuilder orderByField(Enum fieldName) {
            if (fieldName == null)
                this.orderByFieldNameString = "";
            else
                this.orderByFieldNameString = fieldName.name();
            return this;
        }

        public NgramsListQueryBuilder order(Enum order) {
            if (order == null)
                this.orderString = "";
            else
                this.orderString = order.name();
            return this;
        }

        public NgramsListQueryObject build() throws QueryObjectBuilderValidationError {
            if (amount < 1) {
                throw new QueryObjectBuilderValidationError("Amount of items in list is negative or zero.");
            }

            if (offsetPage < 0) {
                throw new QueryObjectBuilderValidationError("Offset page number is negative.");
            }

            try {
                orderByFieldName = NgramsSortFieldName.valueOf(orderByFieldNameString);
            } catch (Exception e) {
                orderByFieldName = null;
                //throw new QueryObjectBuilderValidationError("Field name to order is incorrect.");
            }

            try {
                order = Order.valueOf(orderString);
            } catch (Exception e) {
                order = null;
                //throw new QueryObjectBuilderValidationError("Order tag is incorrect.");
            }

            return new NgramsListQueryObject(this);
        }
    }

    //required
    private final int amount;
    private final int offsetPage;

    //optional
    private final NgramsSortFieldName orderByFieldName;
    private final Order order;

    private NgramsListQueryObject(NgramsListQueryBuilder ngramsListQueryBuilder) {
        this.amount = ngramsListQueryBuilder.amount;
        this.offsetPage = ngramsListQueryBuilder.offsetPage;
        this.orderByFieldName = ngramsListQueryBuilder.orderByFieldName;
        this.order = ngramsListQueryBuilder.order;
    }

    public int getAmount() {
        return amount;
    }

    public int getOffsetPage() {
        return offsetPage;
    }

    public NgramsSortFieldName getOrderByFieldName() {
        return orderByFieldName;
    }

    public Order getOrder() {
        return order;
    }
}