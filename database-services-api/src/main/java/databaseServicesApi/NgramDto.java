package databaseServicesApi;

/**
 * It's supposed that object gets validated data. Object doesn't do validation.
 */
public class NgramDto {
    private final String data;
    private final int count;

    public NgramDto(String data, int count) {
        this.data = data;
        this.count = count;
    }

    public String getData() {
        return data;
    }

    public int getCount() {
        return count;
    }
}
