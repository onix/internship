package databaseServicesApi;

import org.apache.commons.lang3.StringUtils;

public class WordsListQueryObject {
    public static class WordsListQueryBuilder {
        //required
        private String ngramData;
        private int amountOfElements;
        private int offsetPage;
        private String wordToSearchPattern;

        //optional
        private String orderByFieldNameString;
        private WordsSortFieldName orderByFieldName;
        private String orderString;
        private Order order;

        public WordsListQueryBuilder(String ngramData, int amountOfElements, int offsetPage, String wordToSearchPattern) {
            this.ngramData = ngramData;
            this.amountOfElements = amountOfElements;
            this.offsetPage = offsetPage;
            this.wordToSearchPattern = wordToSearchPattern;
        }

        public WordsListQueryBuilder orderByField(Enum fieldName) {
            if (fieldName == null)
                this.orderByFieldNameString = "";
            else
                this.orderByFieldNameString = fieldName.name();
            return this;
        }

        public WordsListQueryBuilder order(Enum order) {
            if (order == null)
                this.orderString = "";
            else
                this.orderString = order.name();
            return this;
        }

        public WordsListQueryObject build() throws QueryObjectBuilderValidationError {
            if (StringUtils.isEmpty(ngramData)) {
                throw new QueryObjectBuilderValidationError("N-gram data is not defined.");
            }

            if (amountOfElements < 1) {
                throw new QueryObjectBuilderValidationError("Amount of items in list is negative or zero.");
            }

            if (offsetPage < 0) {
                throw new QueryObjectBuilderValidationError("Offset page number is negative.");
            }

            if (StringUtils.isEmpty(wordToSearchPattern)) {
                throw new QueryObjectBuilderValidationError("Word search pattern is null or empty.");
            }

            try {
                orderByFieldName = WordsSortFieldName.valueOf(orderByFieldNameString);
            } catch (Exception e) {
                orderByFieldName = null;
                //throw new QueryObjectBuilderValidationError("Field name to order is incorrect.");
            }

            try {
                order = Order.valueOf(orderString);
            } catch (Exception e) {
                order = null;
                //throw new QueryObjectBuilderValidationError("Order tag is incorrect.");
            }

            return new WordsListQueryObject(this);
        }
    }

    //required
    private final int amountOfElements;
    private final int offsetPage;
    private String ngramData;
    private String wordToSearchPattern;

    //optional
    private final WordsSortFieldName orderByFieldName;
    private final Order order;

    private WordsListQueryObject(WordsListQueryBuilder wordsListQueryBuilder) {
        this.amountOfElements = wordsListQueryBuilder.amountOfElements;
        this.offsetPage = wordsListQueryBuilder.offsetPage;
        this.ngramData = wordsListQueryBuilder.ngramData;
        this.wordToSearchPattern = wordsListQueryBuilder.wordToSearchPattern;
        this.orderByFieldName = wordsListQueryBuilder.orderByFieldName;
        this.order = wordsListQueryBuilder.order;
    }

    public int getAmountOfElements() {
        return amountOfElements;
    }

    public int getOffsetPage() {
        return offsetPage;
    }

    public String getNgramData() {
        return ngramData;
    }

    public String getWordToSearchPattern() {
        return wordToSearchPattern;
    }

    public WordsSortFieldName getOrderByFieldName() {
        return orderByFieldName;
    }

    public Order getOrder() {
        return order;
    }
}
