package databaseServicesApi;

import java.util.List;

public interface NgramsDatabaseReaderInterface {
    NgramDto searchNgramByDataFullMatch(String ngramData);

    List<NgramDto> getListOfNgrams(NgramsListQueryObject ngramsListQuery);

    int getAmountOfNgramsInDatabase();
}
