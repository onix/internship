package databaseServicesApi;

public class QueryObjectBuilderValidationError extends Exception {
    public QueryObjectBuilderValidationError(String message) {
        super(message);
    }
}
