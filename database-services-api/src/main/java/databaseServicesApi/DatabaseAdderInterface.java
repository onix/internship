package databaseServicesApi;

import libngram.PhraseDataHolder;

import java.sql.SQLException;

public interface DatabaseAdderInterface {
    void addToDatabase(PhraseDataHolder data) throws Exception;

    void closeDatabaseConnection() throws SQLException;
}
