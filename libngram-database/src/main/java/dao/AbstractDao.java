package dao;

import dao.jdbcDao.IllegalInputSizeException;

import java.sql.*;

public abstract class AbstractDao {
    private final Connection sourceConnection;

    protected AbstractDao(Connection sourceConnection) {
        this.sourceConnection = sourceConnection;
    }

    public Connection getSourceConnection() {
        return sourceConnection;
    }

    public interface Processor<T> {
        T extractData(ResultSet rs) throws SQLException, IllegalInputSizeException;
    }

    private static void initPs(PreparedStatement statement, Object[] params) throws SQLException {
        int i = 0;
        for (Object param : params) {
            statement.setObject(++i, param);
        }
    }

    public <T> T querySelect(String sql, Object[] params, Processor<T> process) throws SQLException, IllegalInputSizeException {
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            statement = sourceConnection.prepareStatement(sql);

            initPs(statement, params);
            rs = statement.executeQuery();
            return process.extractData(rs);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ignored) {
            }

            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception ignored) {
            }
        }
    }

    public <T> T queryInsert(String sql, Object[] params, Processor<T> process) throws SQLException, IllegalInputSizeException {
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            statement = sourceConnection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            initPs(statement, params);
            statement.executeUpdate();
            rs = statement.getGeneratedKeys();
            return process.extractData(rs);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ignored) {
            }

            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception ignored) {
            }
        }
    }

    public void queryUpdate(String sql, Object[] params) throws SQLException, IllegalInputSizeException {
        PreparedStatement statement = null;

        try {
            statement = sourceConnection.prepareStatement(sql);
            initPs(statement, params);
            statement.executeUpdate();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception ignored) {
            }
        }
    }

    public int countElementsQuery(String sql) throws SQLException {
        PreparedStatement statement = null;
        ResultSet count = null;

        try {
            statement = getSourceConnection().prepareStatement(sql);
            count = statement.executeQuery();

            count.next();
            return count.getInt(1);
        } finally {
            try {
                if (count != null) {
                    count.close();
                }
            } catch (Exception ignored) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception ignored) {
            }
        }
    }

    public void clearTableQuery(String sql) throws SQLException {
        PreparedStatement statement = null;

        try {
            statement = getSourceConnection().prepareStatement(sql);
            statement.executeUpdate();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception ignored) {
            }
        }
    }
}
