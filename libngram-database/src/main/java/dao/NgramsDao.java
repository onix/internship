package dao;

import dao.daoQueryObjects.NgramsListDaoQueryObject;
import dao.entities.Ngram;

import java.util.List;

public interface NgramsDao {

    Ngram findByDataString(String ngramDataString);

    int getIdForNgram(String ngramDataString);

    List<Ngram> extractListOfNgrams(NgramsListDaoQueryObject ngramsListDaoQueryObject);

    int extractAmountOfNgrams();

    int insertNewNgram(String ngramString);

    void updateNgramCounter(Ngram ngram);

    void removeAllEntetiesFromTable();
}
