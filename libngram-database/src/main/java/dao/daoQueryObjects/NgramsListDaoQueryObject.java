package dao.daoQueryObjects;

public class NgramsListDaoQueryObject {
    //required
    private int amount;
    private int offsetPage;

    //optional
    private NgramsSortFieldName orderByFieldName;
    private Order order;

    public NgramsListDaoQueryObject(int amount, int offsetPage, Enum orderByFieldName, Enum order) {
        this.amount = amount;
        this.offsetPage = offsetPage;

        if (orderByFieldName == null)
            this.orderByFieldName = null;
        else
            this.orderByFieldName = NgramsSortFieldName.valueOf(orderByFieldName.name());

        if (order == null)
            this.order = null;
        else
            this.order = Order.valueOf(order.name());
    }

    public int getAmount() {
        return amount;
    }

    public int getOffsetPage() {
        return offsetPage;
    }

    public NgramsSortFieldName getOrderByFieldName() {
        return orderByFieldName;
    }

    public Order getOrder() {
        return order;
    }
}
