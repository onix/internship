package dao.entities;

public class WordForNgramWithCount {
    private String word;
    private int count;

    public WordForNgramWithCount(String word, int count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public int getCount() {
        return count;
    }
}
