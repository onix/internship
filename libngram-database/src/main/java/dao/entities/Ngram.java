package dao.entities;

import dao.jdbcDao.IllegalInputSizeException;

public class Ngram {
    private static final int MAXIMUM_NGRAM_STRING_LENGTH = 20;

    public static final String NGRAM_ID_COLUMN_NAME = "id";
    public static final String NGRAM_DATA_COLUMN_NAME = "n_gram_data";
    public static final String NGRAM_COUNT_COLUMN_NAME = "n_gram_count";

    private int id;
    private final String data;
    private int count = 0;

    public Ngram(String ngramData) throws IllegalInputSizeException {
        validateData(ngramData);

        this.data = ngramData;
    }

    public Ngram(int ngramId, String ngramData, int ngramCount) throws IllegalInputSizeException {
        this(ngramData);

        validateCount(ngramCount);
        this.id = ngramId;
        this.count = ngramCount;
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public int getCount() {
        return count;
    }

    public void incrementCounter() {
        count++;
    }

    private void validateData(String ngramData) throws IllegalInputSizeException {
        if (ngramData == null || ngramData.isEmpty()) {
            throw new IllegalArgumentException("Ngram data cannot be null or empty.");
        }

        if (ngramData.length() < 0 && ngramData.length() > MAXIMUM_NGRAM_STRING_LENGTH) {
            throw new IllegalInputSizeException("Ngram string length exceeded allowed " + MAXIMUM_NGRAM_STRING_LENGTH + " symbols limit.");
        }
    }

    private void validateCount(int ngramCount) {
        if (ngramCount <= 0) {
            throw new IllegalArgumentException("Ngram counter can't have negative value.");
        }
    }
}
