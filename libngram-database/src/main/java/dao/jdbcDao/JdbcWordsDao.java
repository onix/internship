package dao.jdbcDao;

import dao.AbstractDao;
import dao.WordsDao;
import dao.daoQueryObjects.WordsListDaoQueryObject;
import dao.entities.WordForNgramWithCount;
import dao.daoQueryObjects.Order;
import dao.daoQueryObjects.WordsSortFieldName;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcWordsDao extends AbstractDao implements WordsDao {
    private final static String SELECT_WORD_BY_ID = "SELECT * FROM WORDS WHERE word_data = ?";
    private final static String INSERT_WORD = "INSERT INTO WORDS(WORD_DATA) VALUES (?)";
    private final static String CLEAN_WORDS_TABLE = "DELETE FROM WORDS";

    private final static String COUNT_TEN_WORDS_FOR_NGRAM_PARTIAL_MATCH =
            "SELECT COUNT(*) FROM WORDS INNER JOIN N_GRAMS_TO_WORDS ON ID = WORD_ID WHERE NGRAM_ID = ? AND WORD_DATA REGEXP ?";
    //------------------------------------------------------------------------------------------------------------------
    private final static String SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART =
            "SELECT WORD_DATA, NGRAM_IN_WORD_COUNT FROM WORDS AS A INNER JOIN N_GRAMS_TO_WORDS AS B ON A.ID = B.WORD_ID WHERE NGRAM_ID = ? AND WORD_DATA REGEXP ? ";

    private final static String SELECT_TEN_WORDS_QUERY_DATA_FIELD_NAME_PART = " WORD_DATA ";
    private final static String SELECT_TEN_WORDS_QUERY_COUNTER_FIELD_PART = " NGRAM_IN_WORD_COUNT ";
    //******************************************************************************************************************
    private final static String QUERY_ORDER_BY_CLAUSE = " ORDER BY ";
    private final static String QUERY_LIMITATION_PART = " LIMIT ? OFFSET ? * 10;";

    //******************************************************************************************************************
    public JdbcWordsDao(Connection sourceConnection) {
        super(sourceConnection);
    }

    @Override
    public int findWord(String wordDataString) {
        if (wordDataString == null)
            return 0;
        try {
            return querySelect(SELECT_WORD_BY_ID, new Object[]{wordDataString},
                    new Processor<Integer>() {
                        @Override
                        public Integer extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            if (rs.first()) {
                                return rs.getInt("id");
                            } else {
                                return 0;
                            }
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    private List<WordForNgramWithCount> extractListOfItems(String query, int ngramId, String partOfWord, int amountOfItems, int offsetPage) {
        if (partOfWord == null || ngramId < 0 || offsetPage < 0)
            return null;
        try {
            return querySelect(query, new Object[]{ngramId, partOfWord, amountOfItems, offsetPage},
                    new Processor<List<WordForNgramWithCount>>() {
                        @Override
                        public List<WordForNgramWithCount> extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            List<WordForNgramWithCount> result = new ArrayList<WordForNgramWithCount>();

                            while (rs.next()) {
                                result.add(new WordForNgramWithCount(
                                        rs.getString("WORD_DATA"),
                                        rs.getInt("NGRAM_IN_WORD_COUNT")
                                ));
                            }
                            if (result.size() == 0) {
                                return null;
                            }
                            return result;
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<WordForNgramWithCount> findAndExtractListOfWordsForNgramByPartialWordMatch(WordsListDaoQueryObject wordsListDaoQueryObject) {
        if (wordsListDaoQueryObject == null) {
            return null;
        }
        String querySB;

        int ngramId = wordsListDaoQueryObject.getNgramId();
        String partOfWord = wordsListDaoQueryObject.getPartOfWord();
        int amountOfItems = wordsListDaoQueryObject.getAmountOfItems();
        int offsetPage = wordsListDaoQueryObject.getOffsetPage();

        if (wordsListDaoQueryObject.getOrder() == null && wordsListDaoQueryObject.getOrderByFieldName() == null) {
            querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART + QUERY_LIMITATION_PART;
            return extractListOfItems(querySB, ngramId, partOfWord, amountOfItems, offsetPage);
        }

        if (wordsListDaoQueryObject.getOrder() == Order.asc) {
            if (wordsListDaoQueryObject.getOrderByFieldName() == WordsSortFieldName.word) { // ASC by word
                querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART +
                        QUERY_ORDER_BY_CLAUSE + SELECT_TEN_WORDS_QUERY_DATA_FIELD_NAME_PART + "ASC" + QUERY_LIMITATION_PART;
                return extractListOfItems(querySB, ngramId, partOfWord, amountOfItems, offsetPage);
            } else { // ASC by count
                querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART +
                        QUERY_ORDER_BY_CLAUSE + SELECT_TEN_WORDS_QUERY_COUNTER_FIELD_PART + "ASC" + QUERY_LIMITATION_PART;
                return extractListOfItems(querySB, ngramId, partOfWord, amountOfItems, offsetPage);
            }
        } else {
            if (wordsListDaoQueryObject.getOrderByFieldName() == WordsSortFieldName.word) { // DESC by word
                querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART +
                        QUERY_ORDER_BY_CLAUSE + SELECT_TEN_WORDS_QUERY_DATA_FIELD_NAME_PART + "DESC" + QUERY_LIMITATION_PART;
                return extractListOfItems(querySB, ngramId, partOfWord, amountOfItems, offsetPage);
            } else { // DESC by count
                querySB = SELECT_TEN_WORDS_WITH_COUNTERS_FOR_NGRAM_BY_PARTIAL_WORD_DATA_GENERAL_PART +
                        QUERY_ORDER_BY_CLAUSE + SELECT_TEN_WORDS_QUERY_COUNTER_FIELD_PART + "DESC" + QUERY_LIMITATION_PART;
                return extractListOfItems(querySB, ngramId, partOfWord, amountOfItems, offsetPage);
            }
        }
    }

    @Override
    public int extractAmountOfWordsForNgramByItsDataPartialMatch(int ngramData, String partOfWord) {
        if (ngramData > 0 || partOfWord != null) {
            try {
                return querySelect(COUNT_TEN_WORDS_FOR_NGRAM_PARTIAL_MATCH, new Object[]{ngramData, partOfWord},
                        new Processor<Integer>() {
                            @Override
                            public Integer extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                                if (rs.first()) {
                                    return rs.getInt(1);
                                } else {
                                    return 0;
                                }
                            }
                        }
                );
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (IllegalInputSizeException e) {
                throw new RuntimeException(e);
            }
        }
        return 0;
    }

    @Override
    public int insertNewWord(String wordString, int phraseDbId) {
        if (wordString == null)
            return 0;
        try {
            return queryInsert(INSERT_WORD, new Object[]{wordString},
                    new Processor<Integer>() {
                        @Override
                        public Integer extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            rs.next();
                            return rs.getInt(1);
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void removeAllEntetiesFromTable() {
        try {
            clearTableQuery(CLEAN_WORDS_TABLE);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
