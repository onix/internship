package dao.jdbcDao;

import dao.AbstractDao;
import dao.PhrasesDao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcPhrasesDao extends AbstractDao implements PhrasesDao {
    //private final static String SELECT_PHRASE_BY_ID = "SELECT * FROM PHRASES WHERE id = ?";
    private final static String INSERT_PHRASE = "INSERT INTO PHRASES(PHRASE_DATA) VALUES(?)";
    private final static String CLEAN_PHRASES_TABLE = "DELETE FROM PHRASES";

    public JdbcPhrasesDao(Connection sourceConnection) {
        super(sourceConnection);
    }

    @Override
    public int insertNewPhrase(String phraseString) {
        if (phraseString == null)
            return 0;
        try {
            return queryInsert(INSERT_PHRASE, new Object[]{phraseString},
                    new Processor<Integer>() {
                        @Override
                        public Integer extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            rs.next();
                            return rs.getInt(1);
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void removeAllEntetiesFromTable() {
        try {
            clearTableQuery(CLEAN_PHRASES_TABLE);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
