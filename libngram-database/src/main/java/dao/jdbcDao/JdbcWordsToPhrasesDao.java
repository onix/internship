package dao.jdbcDao;

import dao.AbstractDao;
import dao.WordsToPhrasesDao;
import dao.entities.WordToPhrase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcWordsToPhrasesDao extends AbstractDao implements WordsToPhrasesDao {
    private final static String SELECT_WORD_TO_PHRASE = "SELECT * FROM WORDS_TO_PHRASES WHERE WORD_ID = ? AND PHRASE_ID = ?";
    private final static String INSERT_NEW_WORD_TO_PHRASE = "INSERT INTO WORDS_TO_PHRASES VALUES (?, ?, 1);";
    private final static String UPDATE_WORDS_TO_PHRASES_COUNTER = "UPDATE WORDS_TO_PHRASES SET WORDS_IN_PHRASE_COUNT = ? WHERE WORD_ID = ? AND PHRASE_ID = ?";
    private final static String CLEAN_WORDS_TO_PHRASES_TABLE = "DELETE FROM WORDS_TO_PHRASES;";

    public JdbcWordsToPhrasesDao(Connection sourceConnection) {
        super(sourceConnection);
    }

    @Override
    public WordToPhrase findWordToPhrase(int wordId, int phraseId) {
        if (wordId == 0 || phraseId == 0)
            return null;
        try {
            return querySelect(SELECT_WORD_TO_PHRASE, new Object[]{wordId, phraseId},
                    new Processor<WordToPhrase>() {
                        @Override
                        public WordToPhrase extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            if (rs.first()) {
                                return new WordToPhrase(
                                        rs.getInt(WordToPhrase.WORD_TO_PHRASE_WORD_ID_COLUMN_NAME),
                                        rs.getInt(WordToPhrase.WORD_TO_PHRASE_PHRASE_ID_COLUMN_NAME),
                                        rs.getInt(WordToPhrase.WORD_TO_PHRASE_WORDS_IN_PHRASE_COUNT_COLUMN_NAME)
                                );
                            } else {
                                return null;
                            }
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public WordToPhrase insertNewWordToPhrase(final int wordId, final int phraseId) {
        if (wordId == 0 && phraseId == 0)
            return null;
        try {
            return queryInsert(INSERT_NEW_WORD_TO_PHRASE, new Object[]{wordId, phraseId},
                    new Processor<WordToPhrase>() {
                        @Override
                        public WordToPhrase extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            return new WordToPhrase(wordId, phraseId, 1);
                        }
                    }
            );

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateWordToPhraseCounter(WordToPhrase wordToPhrase) {
        if (wordToPhrase != null) {
            try {
                queryUpdate(UPDATE_WORDS_TO_PHRASES_COUNTER, new Object[]{wordToPhrase.getWordToPhraseCount(), wordToPhrase.getWordId(), wordToPhrase.getPhraseId()});
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (IllegalInputSizeException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void removeAllEntetiesFromTable() {
        try {
            clearTableQuery(CLEAN_WORDS_TO_PHRASES_TABLE);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
