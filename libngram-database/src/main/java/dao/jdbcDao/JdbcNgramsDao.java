package dao.jdbcDao;

import dao.AbstractDao;
import dao.NgramsDao;
import dao.daoQueryObjects.NgramsListDaoQueryObject;
import dao.entities.Ngram;
import dao.daoQueryObjects.NgramsSortFieldName;
import dao.daoQueryObjects.Order;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcNgramsDao extends AbstractDao implements NgramsDao {
    private final static String SELECT_NGRAM_BY_NGRAM_STRING_DATA = "SELECT * FROM N_GRAMS WHERE N_GRAM_DATA = ?";
    private final static String SELECT_AMOUNT_OF_ELEMENTS = "SELECT COUNT(*) FROM N_GRAMS";
    private final static String INSERT_NEW_NGRAM = "INSERT INTO N_GRAMS(N_GRAM_DATA, N_GRAM_COUNT) VALUES (?, 1)";
    private final static String UPDATE_NGRAM_COUNTER = "UPDATE N_GRAMS SET N_GRAM_COUNT = ? WHERE ID = ?";
    private final static String CLEAN_NGRAMS_TABLE = "DELETE FROM N_GRAMS";
    //------------------------------------------------------------------------------------------------------------------
    private final static String SELECT_TEN_NGRAMS_GENERAL_PART = "SELECT * FROM N_GRAMS ";

    private final static String SELECT_TEN_NGRAMS_QUERY_DATA_FIELD_NAME_PART = " n_gram_data ";
    private final static String SELECT_TEN_NGRAMS_QUERY_COUNTER_FIELD_NAME_PART = " n_gram_count ";

    private final static String QUERY_ORDER_BY_CLAUSE = " ORDER BY ";
    private final static String QUERY_LIMITATION_PART = " LIMIT ? OFFSET ? * 10;";
    //******************************************************************************************************************
    public JdbcNgramsDao(Connection sourceConnection) {
        super(sourceConnection);
    }

    @Override
    public Ngram findByDataString(String ngramDataString) {
        if (ngramDataString == null)
            return null;
        try {
            return querySelect(SELECT_NGRAM_BY_NGRAM_STRING_DATA, new Object[]{ngramDataString},
                    new Processor<Ngram>() {
                        @Override
                        public Ngram extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            if (rs.first()) {
                                return new Ngram(
                                        rs.getInt(Ngram.NGRAM_ID_COLUMN_NAME),
                                        rs.getString(Ngram.NGRAM_DATA_COLUMN_NAME),
                                        rs.getInt(Ngram.NGRAM_COUNT_COLUMN_NAME)
                                );
                            } else {
                                return null;
                            }
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int getIdForNgram(String ngramDataString) {
        Ngram ngram = findByDataString(ngramDataString);
        if (ngram == null) {
            return 0;
        }
        return ngram.getId();
    }

    private List<Ngram> extractListOfItems(String query, int amountOfItems, int offsetPage) {
        if (offsetPage < 0 || amountOfItems < 1)
            return null;
        try {
            return querySelect(query, new Object[]{amountOfItems, offsetPage},
                    new Processor<List<Ngram>>() {
                        @Override
                        public List<Ngram> extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            List<Ngram> result = new ArrayList<Ngram>();

                            while (rs.next()) {
                                result.add(new Ngram(
                                        rs.getInt(Ngram.NGRAM_ID_COLUMN_NAME),
                                        rs.getString(Ngram.NGRAM_DATA_COLUMN_NAME),
                                        rs.getInt(Ngram.NGRAM_COUNT_COLUMN_NAME)
                                ));
                            }
                            if (result.size() == 0) {
                                return null;
                            }
                            return result;
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Ngram> extractListOfNgrams(NgramsListDaoQueryObject ngramsListDaoQueryObject) {
        if (ngramsListDaoQueryObject == null) {
            return null;
        }

        String querySB;

        int amountOfItems = ngramsListDaoQueryObject.getAmount();
        int offsetPage = ngramsListDaoQueryObject.getOffsetPage();

        if (ngramsListDaoQueryObject.getOrder() == null && ngramsListDaoQueryObject.getOrderByFieldName() == null) {
            querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_LIMITATION_PART;
            return extractListOfItems(querySB, amountOfItems, offsetPage);
        }

        if (ngramsListDaoQueryObject.getOrder() == Order.asc) {
            if (ngramsListDaoQueryObject.getOrderByFieldName() == NgramsSortFieldName.name) { // ASC by name
                querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_ORDER_BY_CLAUSE + SELECT_TEN_NGRAMS_QUERY_DATA_FIELD_NAME_PART + "ASC" + QUERY_LIMITATION_PART;
                return extractListOfItems(querySB, amountOfItems, offsetPage);
            } else { // ASC by count
                querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_ORDER_BY_CLAUSE + SELECT_TEN_NGRAMS_QUERY_COUNTER_FIELD_NAME_PART + "ASC" + QUERY_LIMITATION_PART;
                return extractListOfItems(querySB, amountOfItems, offsetPage);
            }
        } else {
            if (ngramsListDaoQueryObject.getOrderByFieldName() == NgramsSortFieldName.name) { // DESC by name
                querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_ORDER_BY_CLAUSE + SELECT_TEN_NGRAMS_QUERY_DATA_FIELD_NAME_PART + "DESC" + QUERY_LIMITATION_PART;
                return extractListOfItems(querySB, amountOfItems, offsetPage);
            } else { // DESC by count
                querySB = SELECT_TEN_NGRAMS_GENERAL_PART + QUERY_ORDER_BY_CLAUSE + SELECT_TEN_NGRAMS_QUERY_COUNTER_FIELD_NAME_PART + "DESC" + QUERY_LIMITATION_PART;
                return extractListOfItems(querySB, amountOfItems, offsetPage);
            }
        }
    }

    @Override
    public int extractAmountOfNgrams() {
        try {
            return countElementsQuery(SELECT_AMOUNT_OF_ELEMENTS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int insertNewNgram(String ngramString) {
        if (ngramString == null)
            return 0;
        try {
            return queryInsert(INSERT_NEW_NGRAM, new Object[]{ngramString},
                    new Processor<Integer>() {
                        @Override
                        public Integer extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            rs.next();
                            return rs.getInt(1);
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateNgramCounter(Ngram ngram) {
        if (ngram != null) {
            try {
                queryUpdate(UPDATE_NGRAM_COUNTER, new Object[]{ngram.getCount(), ngram.getId()});
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (IllegalInputSizeException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void removeAllEntetiesFromTable() {
        try {
            clearTableQuery(CLEAN_NGRAMS_TABLE);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
