package dao.jdbcDao;

import dao.AbstractDao;
import dao.NgramsToWordsDao;
import dao.entities.NgramToWord;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcNgramsToWordsDao extends AbstractDao implements NgramsToWordsDao {
    private final static String SELECT_NGRAM_TO_WORD_BY_ID = "SELECT * FROM N_GRAMS_TO_WORDS WHERE NGRAM_ID = ? AND WORD_ID = ?";
    private final static String INSERT_NEW_NGRAM_TO_WORD = "INSERT INTO N_GRAMS_TO_WORDS VALUES (?, ?, 1)";
    private final static String UPDATE_NGRAM_TO_WORD_COUNTER = "UPDATE N_GRAMS_TO_WORDS SET NGRAM_IN_WORD_COUNT = ? WHERE NGRAM_ID = ? AND WORD_ID = ?";
    private final static String CLEAN_NGRAMS_TO_WORDS_TABLE = "DELETE FROM N_GRAMS_TO_WORDS";

    public JdbcNgramsToWordsDao(Connection sourceConnection) {
        super(sourceConnection);
    }

    @Override
    public NgramToWord findNgramToWord(int ngramId, int wordId) {
        if (ngramId == 0 || wordId == 0)
            return null;
        try {
            return querySelect(SELECT_NGRAM_TO_WORD_BY_ID, new Object[]{ngramId, wordId},
                    new Processor<NgramToWord>() {
                        @Override
                        public NgramToWord extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            if (rs.first()) {
                                return new NgramToWord(
                                        rs.getInt(NgramToWord.NGRAM_TO_WORD_NGRAM_ID_COLUMN_NAME),
                                        rs.getInt(NgramToWord.NGRAM_TO_WORD_WORD_ID_COLUMN_NAME),
                                        rs.getInt(NgramToWord.NGRAM_TO_WORD_NGRAM_IN_WORD_COUNT_COLUMN_NAME)
                                );
                            } else {
                                return null;
                            }
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int insertNewNgramsToWords(int ngramId, int wordId) {
        if (ngramId == 0 && wordId == 0)
            return 0;
        try {
            return queryInsert(INSERT_NEW_NGRAM_TO_WORD, new Object[]{ngramId, wordId},
                    new Processor<Integer>() {
                        @Override
                        public Integer extractData(ResultSet rs) throws SQLException, IllegalInputSizeException {
                            rs.next();
                            return 1;
                        }
                    }
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalInputSizeException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateNgramToWordCounter(NgramToWord ngramToWord) {
        if (ngramToWord != null) {
            try {
                queryUpdate(UPDATE_NGRAM_TO_WORD_COUNTER, new Object[]{ngramToWord.getNgramInWordCount(), ngramToWord.getNgramId(), ngramToWord.getWordId()});
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (IllegalInputSizeException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void removeAllEntetiesFromTable() {
        try {
            clearTableQuery(CLEAN_NGRAMS_TO_WORDS_TABLE);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
