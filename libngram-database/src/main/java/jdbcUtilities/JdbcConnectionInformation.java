package jdbcUtilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class JdbcConnectionInformation {
    private final static String jdbcConnectionStringFilePath = "jdbc.properties";

    public static Properties getJdbcConnectionInformation() throws IOException {
        Properties connectionInfo = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream(jdbcConnectionStringFilePath);
        connectionInfo.load(stream);

        return connectionInfo;
    }
}
