package jdbcUtilities;

import java.io.IOException;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcH2DataSource {
    private static Properties connectionProperties;
    private static URI connectionString;

    public static Connection getDBConnection() {
        try {
            if (connectionProperties == null) {
                synchronized (JdbcH2DataSource.class) {
                    if (connectionProperties == null) {
                        getConnectionInformation();
                    }
                }
            }

            Class.forName(connectionProperties.getProperty("DB_DRIVER"));
            return DriverManager.getConnection(connectionString.toString(), connectionProperties.getProperty("DB_USER"), connectionProperties.getProperty("DB_PASSWORD"));
        } catch (ClassNotFoundException e) {
            System.out.println("JDBC driver not found.");
            throw new RuntimeException(e);
        } catch (SQLException e) {
            System.out.println("Connection to database failed.");
            throw new RuntimeException(e);
        }
    }

    private static void getConnectionInformation() {
        try {
            connectionProperties = JdbcConnectionInformation.getJdbcConnectionInformation();
            String cleanURI = connectionProperties.getProperty("CONNECTION_URL");
            connectionString = URI.create(cleanURI);
        } catch (IOException e) {
            System.out.println("JDBC connection information couldn't be read. Wront file I/O operation.");
            System.exit(-1);
        }
    }
}
