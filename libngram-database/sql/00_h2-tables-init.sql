-- Create independent entities
DROP TABLE IF EXISTS n_grams;
CREATE TABLE n_grams(
  id INT PRIMARY KEY auto_increment NOT NULL,
  n_gram_data VARCHAR(20) NOT NULL,
  n_gram_count INT NOT NULL,
  CONSTRAINT n_gram_data_unique UNIQUE(n_gram_data)
);

DROP TABLE IF EXISTS words;
CREATE TABLE words(
  id INT PRIMARY KEY auto_increment NOT NULL,
  word_data VARCHAR(64) NOT NULL,
  CONSTRAINT word_data_unique UNIQUE(word_data)
);

DROP TABLE IF EXISTS phrases;
CREATE TABLE phrases(
  id INT PRIMARY KEY auto_increment NOT NULL,
  phrase_data VARCHAR(255) NOT NULL
);

-- Create entities many-to-many link implementation
DROP TABLE IF EXISTS n_grams_to_words;
CREATE TABLE n_grams_to_words(
  ngram_id INT NOT NULL,
  word_id INT NOT NULL,
  ngram_in_word_count INT NOT NULL,
  PRIMARY KEY(ngram_id, word_id),
  CONSTRAINT n_grams_to_words_ngram_fk FOREIGN KEY (ngram_id) REFERENCES n_grams(id) ON DELETE CASCADE,
  CONSTRAINT n_grams_to_words_word_fk FOREIGN KEY (word_id) REFERENCES words(id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS words_to_phrases;
CREATE TABLE words_to_phrases(
  word_id INT NOT NULL,
  phrase_id INT NOT NULL,
  words_in_phrase_count INT NOT NULL,
  PRIMARY KEY(word_id, phrase_id),
  CONSTRAINT words_to_phrases_word_fk FOREIGN KEY (word_id) REFERENCES words(id) ON DELETE CASCADE,
  CONSTRAINT words_to_phrases_phrase_fk FOREIGN KEY (phrase_id) REFERENCES phrases(id) ON DELETE CASCADE
);