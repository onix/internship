INSERT INTO n_grams VALUES
(1, 'red', 2),
(2, 'jea', 1),
(3, 'ean', 1),
(4, 'ans', 1),
(5, 'lov', 1),
(6, 'ove', 1),
(7, 'edi', 1),
(8, 'dis', 1);

INSERT INTO words VALUES
(1, 'red'),
(2, 'jeans'),
(3, 'i'),
(4, 'love'),
(5, 'redis');

INSERT INTO phrases VALUES
(1, 'red jeans'),
(2, 'i love redis');

INSERT INTO n_grams_to_words VALUES
(1, 1, 1),
(2, 2, 1),
(3, 2, 1),
(4, 2, 1),
(5, 4, 1),
(6, 4, 1),
(1, 5, 1),
(7, 5, 1),
(8, 5, 1);

INSERT INTO words_to_phrases VALUES
(1, 1, 1),
(2, 1, 1),
(3, 2, 1),
(4, 2, 1),
(5, 2, 1);