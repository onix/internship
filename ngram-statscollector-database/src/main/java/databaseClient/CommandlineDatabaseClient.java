package databaseClient;

import commandlineCommon.CommandlineClientsShared;
import libngram.PhrasesWithWordsAndNgramsFromFileExtractor;
import libngram.PhraseDataHolder;
import commandlineCommon.CommandlineArgumentException;
import databaseServicesImplementation.DatabaseAdder;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CommandlineDatabaseClient {

    public static void main(String[] args) {
        PhrasesWithWordsAndNgramsFromFileExtractor extractor = null;
        DatabaseAdder dbAdd = null;

        try {
            CommandlineClientsShared.checkInputParameters(args);

            final int ngramLength = Integer.parseInt(args[0]);
            final String fileName = args[1];

            dbAdd = new DatabaseAdder();

            extractor = new PhrasesWithWordsAndNgramsFromFileExtractor(fileName, ngramLength);

            Runnable notifier = new Runnable() {
                public void run() {
                    System.out.print(".");
                }
            };

            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

            System.out.println("Database filling task started.");
            scheduler.scheduleAtFixedRate(notifier, 1, 1, TimeUnit.SECONDS);

            dbAdd.clearDatabase();

            long startTime = System.currentTimeMillis();
            for (PhraseDataHolder data : extractor) {
                dbAdd.addToDatabase(data);
            }
            long endTime = System.currentTimeMillis();
            long totalRunningTime = (endTime - startTime) / 1000;
            scheduler.shutdownNow();

            System.out.println("Success!");
            System.out.println("Finished in " + totalRunningTime + " sec.");
        } catch (CommandlineArgumentException e) {
            System.out.println(e.getMessage());
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (SQLException e) {
            System.out.println("Error in database processing occurred.");
        } catch (Throwable throwable) {
            System.out.println("Unexpected error occurred.");
            throwable.printStackTrace();
        } finally {
            if (extractor != null) {
                extractor.closeFile();
            }
            if (dbAdd != null) {
                try {
                    dbAdd.closeDatabaseConnection();
                } catch (SQLException ignored) {
                }
            }
        }
    }
}
