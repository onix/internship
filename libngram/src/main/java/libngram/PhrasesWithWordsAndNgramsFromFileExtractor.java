package libngram;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PhrasesWithWordsAndNgramsFromFileExtractor implements Iterable<PhraseDataHolder> {
    private final int ngramLength;
    private final BufferedReader fileReader;
    private boolean fileReaderHasIterator;

    public PhrasesWithWordsAndNgramsFromFileExtractor(String pathToFile, int ngramLength) throws FileNotFoundException {
        if (pathToFile == null) {
            throw new IllegalArgumentException("Method got null as 'Path to file' argument.");
        }
        if (ngramLength < 1) {
            throw new IllegalArgumentException("Method got invalid minimal length argument. Must be greater than '1'.");
        }

        this.ngramLength = ngramLength;
        File file = new File(pathToFile);
        fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")));
    }

    @Override
    public Iterator<PhraseDataHolder> iterator() {
        if (fileReaderHasIterator)
            throw new IllegalAccessError("File reader has iterator already.");

        fileReaderHasIterator = true;
        return new Iterator<PhraseDataHolder>() {
            @Override
            public boolean hasNext() {
                try {
                    fileReader.mark(1);
                    if (fileReader.read() < 0) {
                        return false;
                    }
                    fileReader.reset();
                    return true;
                } catch (IOException e) {
                    return false;
                }
            }

            @Override
            public PhraseDataHolder next() {
                try {
                    String phrase = fileReader.readLine();
                    if (!StringUtils.isEmpty(phrase)) {
                        List<WordWithNgrams> wordWithNgramsList = new ArrayList<WordWithNgrams>();

                        // \\s+ means any number of whitespaces between tokens
                        String[] wordsInPhrase = phrase.split("\\s+");

                        for (int i = 0; i < wordsInPhrase.length; i++) {
                            String word = wordsInPhrase[i].toLowerCase();
                            if (word.length() >= ngramLength) {
                                wordWithNgramsList.add(NgramSplitter.splitWordOnNgrams(i, word, ngramLength));
                            } else {
                                wordWithNgramsList.add(new WordWithNgrams(i, word, null));
                            }
                        }
                        return new PhraseDataHolder(phrase.toLowerCase(), wordWithNgramsList);
                    }
                    return null;
                } catch (IOException e) {
                    throw new RuntimeException();
                }
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

        };
    }

    public void closeFile() {
        IOUtils.closeQuietly(fileReader);
    }

    @Override
    protected void finalize() throws Throwable {
        closeFile();
        super.finalize();
    }
}
