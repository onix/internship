package libngram;

import libngram.NgramSplitter;
import libngram.WordWithNgrams;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;

public class NgramSplitterTest {
    private static final String TEST_WORD_ONE = "hello";
    private static final int TEST_WORD_ONE_ID = 0;

    private static final String TEST_WORD_TWO = "wikikipedia";
    private static final int TEST_WORD_TWO_ID = 1;

    private static final WordWithNgrams TEST_WORD_ONE_MODEL =
            new WordWithNgrams(TEST_WORD_ONE_ID, TEST_WORD_ONE,
                    new ArrayList<String>() {{
                        add("he");
                        add("el");
                        add("ll");
                        add("lo");
                    }}
            );

    private static final WordWithNgrams TEST_WORD_TWO_MODEL =
            new WordWithNgrams(TEST_WORD_TWO_ID, TEST_WORD_TWO,
                    new ArrayList<String>() {{
                        add("wi");
                        add("ik");
                        add("ki");
                        add("ik");
                        add("ki");
                        add("ip");
                        add("pe");
                        add("ed");
                        add("di");
                        add("ia");
                    }}
            );

    @DataProvider(name = "testWordVariable")
    public Object[][] createData() throws IOException {
        return new Object[][]{
                {TEST_WORD_ONE_ID, TEST_WORD_ONE, 2, TEST_WORD_ONE_MODEL},
                {TEST_WORD_TWO_ID, TEST_WORD_TWO, 2, TEST_WORD_TWO_MODEL}
        };
    }

    @Test(dataProvider = "testWordVariable")
    public void testSplitWordOneNgrams(int wordInPhraseId, String word, int ngramLength, WordWithNgrams wordWithNgramsModel) throws Exception {
        WordWithNgrams computedWordWithNgrams = NgramSplitter.splitWordOnNgrams(wordInPhraseId, word, ngramLength);

        Assert.assertEquals(computedWordWithNgrams, wordWithNgramsModel);
    }
}
