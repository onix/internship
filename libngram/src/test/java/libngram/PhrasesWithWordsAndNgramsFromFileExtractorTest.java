package libngram;

import libngram.PhraseDataHolder;
import libngram.PhrasesWithWordsAndNgramsFromFileExtractor;
import libngram.WordWithNgrams;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PhrasesWithWordsAndNgramsFromFileExtractorTest {
    private final URL FILE_NAME_TEST_ONE_WORD_LOCATION = this.getClass().getResource("/test-one-word.txt");
    private static final String ONE_WORD_FILE_PHRASE = "combinationcombinatio";
    private static final List<WordWithNgrams> ONE_WORD_FILE_LIST_OF_WORDS_WITH_NGRAMS =
            new ArrayList<WordWithNgrams>() {{
                add(
                        new WordWithNgrams(0, "combinationcombinatio", new ArrayList<String>() {{
                            add("co");
                            add("om");
                            add("mb");
                            add("bi");
                            add("in");
                            add("na");
                            add("at");
                            add("ti");
                            add("io");
                            add("on");
                            add("nc");
                            add("co");
                            add("om");
                            add("mb");
                            add("bi");
                            add("in");
                            add("na");
                            add("at");
                            add("ti");
                            add("io");
                        }})
                );
            }};
    private static final PhraseDataHolder ONE_WORD_FILE_DATA = new PhraseDataHolder(ONE_WORD_FILE_PHRASE, ONE_WORD_FILE_LIST_OF_WORDS_WITH_NGRAMS);

    private final URL FILE_NAME_TEST_TWO_WORDS_LOCATION = this.getClass().getResource("/test-two-words.txt");
    private static final String TWO_WORDS_FILE_PHRASE = "hellocombin combinhi";
    private static final List<WordWithNgrams> TWO_WORDS_FILE_LIST_OF_WORDS_WITH_NGRAMS =
            new ArrayList<WordWithNgrams>() {{
                add(
                        new WordWithNgrams(0, "hellocombin", new ArrayList<String>() {{
                            add("he");
                            add("el");
                            add("ll");
                            add("lo");
                            add("oc");
                            add("co");
                            add("om");
                            add("mb");
                            add("bi");
                            add("in");
                        }})
                );
                add(
                        new WordWithNgrams(1, "combinhi", new ArrayList<String>() {{
                            add("co");
                            add("om");
                            add("mb");
                            add("bi");
                            add("in");
                            add("nh");
                            add("hi");
                        }})
                );
            }};
    private static final PhraseDataHolder TWO_WORDS_FILE_DATA = new PhraseDataHolder(TWO_WORDS_FILE_PHRASE, TWO_WORDS_FILE_LIST_OF_WORDS_WITH_NGRAMS);

    private final URL FILE_NAME_TEST_MULTIPLE_STRINGS_LOCATION = this.getClass().getResource("/test-multiple-strings.txt");
    private static final String MULTIPLE_STRINGS_FILE_PHRASE_ONE = "hello world";
    private static final List<WordWithNgrams> MULTIPLE_STRINGS_FILE_LIST_OF_WORDS_WITH_NGRAMS_PHRASE_ONE =
            new ArrayList<WordWithNgrams>() {{
                add(
                        new WordWithNgrams(0, "hello", new ArrayList<String>() {{
                            add("he");
                            add("el");
                            add("ll");
                            add("lo");
                        }})
                );
                add(
                        new WordWithNgrams(1, "world", new ArrayList<String>() {{
                            add("wo");
                            add("or");
                            add("rl");
                            add("ld");
                        }})
                );
            }};
    private static final PhraseDataHolder MULTIPLE_STRINGS_FILE_DATA_PHRASE_ONE = new PhraseDataHolder(MULTIPLE_STRINGS_FILE_PHRASE_ONE, MULTIPLE_STRINGS_FILE_LIST_OF_WORDS_WITH_NGRAMS_PHRASE_ONE);

    private static final String MULTIPLE_STRINGS_FILE_PHRASE_TWO = "good bye america oooo";
    private static final List<WordWithNgrams> MULTIPLE_STRINGS_FILE_LIST_OF_WORDS_WITH_NGRAMS_PHRASE_TWO =
            new ArrayList<WordWithNgrams>() {{
                add(
                        new WordWithNgrams(0, "good", new ArrayList<String>() {{
                            add("go");
                            add("oo");
                            add("od");
                        }})
                );
                add(
                        new WordWithNgrams(1, "bye", new ArrayList<String>() {{
                            add("by");
                            add("ye");
                        }})
                );
                add(
                        new WordWithNgrams(2, "america", new ArrayList<String>() {{
                            add("am");
                            add("me");
                            add("er");
                            add("ri");
                            add("ic");
                            add("ca");
                        }})
                );
                add(
                        new WordWithNgrams(3, "oooo", new ArrayList<String>() {{
                            add("oo");
                            add("oo");
                            add("oo");
                        }})
                );
            }};

    private static final PhraseDataHolder MULTIPLE_STRINGS_FILE_DATA_PHRASE_TWO = new PhraseDataHolder(MULTIPLE_STRINGS_FILE_PHRASE_TWO, MULTIPLE_STRINGS_FILE_LIST_OF_WORDS_WITH_NGRAMS_PHRASE_TWO);

    @DataProvider(name = "fileVariable")
    public Object[][] createData() throws IOException {
        return new Object[][]{
                {FILE_NAME_TEST_ONE_WORD_LOCATION, 2, ONE_WORD_FILE_DATA},
                {FILE_NAME_TEST_TWO_WORDS_LOCATION, 2, TWO_WORDS_FILE_DATA}
        };
    }

    @Test(dataProvider = "fileVariable")
    public void testGetNextPhraseDataForOneStringFile(URL location, int ngramLength, PhraseDataHolder phraseData) throws Exception {
        PhrasesWithWordsAndNgramsFromFileExtractor extractor = new PhrasesWithWordsAndNgramsFromFileExtractor(location.getPath(), ngramLength);

        Iterator<PhraseDataHolder> iterator = extractor.iterator();
        PhraseDataHolder extractedData = iterator.next();

        Assert.assertEquals(extractedData, phraseData);
        extractor.closeFile();
    }

    @Test
    public void testGetNextPhraseDataForMultipleStringsFile() throws Exception {
        PhrasesWithWordsAndNgramsFromFileExtractor extractor = new PhrasesWithWordsAndNgramsFromFileExtractor(FILE_NAME_TEST_MULTIPLE_STRINGS_LOCATION.getPath(), 2);

        List<PhraseDataHolder> extractedData = new ArrayList<PhraseDataHolder>();

        for (PhraseDataHolder data : extractor) {
            extractedData.add(data);
        }

        Assert.assertEquals(extractedData.get(0), MULTIPLE_STRINGS_FILE_DATA_PHRASE_ONE);
        Assert.assertEquals(extractedData.get(1), MULTIPLE_STRINGS_FILE_DATA_PHRASE_TWO);

        extractor.closeFile();
    }

    @Test(expectedExceptions = IllegalAccessError.class)
    public void testIteratorDuplicate() throws FileNotFoundException {
        PhrasesWithWordsAndNgramsFromFileExtractor extractor = new PhrasesWithWordsAndNgramsFromFileExtractor(FILE_NAME_TEST_MULTIPLE_STRINGS_LOCATION.getPath(), 2);

        extractor.iterator();
        extractor.iterator();
    }
}
