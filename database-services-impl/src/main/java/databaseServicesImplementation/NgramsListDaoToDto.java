package databaseServicesImplementation;

import dao.entities.Ngram;
import databaseServicesApi.NgramDto;

import java.util.ArrayList;
import java.util.List;

public class NgramsListDaoToDto {
    public static List<NgramDto> convert(List<Ngram> listToConvert) {
        List<NgramDto> listToReturn = new ArrayList<NgramDto>(listToConvert.size());

        for (Ngram element : listToConvert) {
            listToReturn.add(new NgramDto(element.getData(), element.getCount()));
        }

        return listToReturn;
    }
}
