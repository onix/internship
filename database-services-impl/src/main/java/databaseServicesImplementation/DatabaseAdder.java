package databaseServicesImplementation;

import dao.*;
import dao.entities.Ngram;
import dao.entities.NgramToWord;
import dao.entities.WordToPhrase;
import dao.jdbcDao.IllegalInputSizeException;
import dao.jdbcDao.*;
import libngram.PhraseDataHolder;
import libngram.WordWithNgrams;
import databaseServicesApi.DatabaseAdderInterface;
import jdbcUtilities.JdbcH2DataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseAdder implements DatabaseAdderInterface {
    private Connection connection;

    private PhrasesDao phrasesDao;
    private WordsDao wordsDao;
    private NgramsDao ngramsDao;
    private WordsToPhrasesDao wordsToPhrasesDao;
    private NgramsToWordsDao ngramsToWordsDao;

    public DatabaseAdder() throws SQLException {
        connection = JdbcH2DataSource.getDBConnection();

        connection.setAutoCommit(false);
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

        phrasesDao = new JdbcPhrasesDao(connection);
        wordsDao = new JdbcWordsDao(connection);
        ngramsDao = new JdbcNgramsDao(connection);

        wordsToPhrasesDao = new JdbcWordsToPhrasesDao(connection);
        ngramsToWordsDao = new JdbcNgramsToWordsDao(connection);
    }

    public void clearDatabase() throws SQLException {
        // Clear data in tables phase
        ngramsToWordsDao.removeAllEntetiesFromTable();
        wordsToPhrasesDao.removeAllEntetiesFromTable();
        ngramsDao.removeAllEntetiesFromTable();
        wordsDao.removeAllEntetiesFromTable();
        phrasesDao.removeAllEntetiesFromTable();

        connection.commit();
    }

    @Override
    public void addToDatabase(PhraseDataHolder data) throws Exception {
        try {
            // Insert phrase into table
            int phraseDbId = phrasesDao.insertNewPhrase(data.getPhrase());

            // Insert Elements into table
            for (WordWithNgrams wordWithNgrams : data.getWordsWithNgramsList()) {
                String currentWordData = wordWithNgrams.getWord();

                boolean isWordNew = false;

                int wordDbId = wordsDao.findWord(currentWordData);
                if (wordDbId == 0) {
                    wordDbId = wordsDao.insertNewWord(currentWordData, phraseDbId);
                    isWordNew = true;
                }

                WordToPhrase wordToPhrase = wordsToPhrasesDao.findWordToPhrase(wordDbId, phraseDbId);
                if (wordToPhrase == null) {
                    wordsToPhrasesDao.insertNewWordToPhrase(wordDbId, phraseDbId);
                } else {
                    wordToPhrase.incrementCounter();
                    wordsToPhrasesDao.updateWordToPhraseCounter(wordToPhrase);
                }

                if (wordWithNgrams.getNgramsList() != null) {
                    for (String currentNgramData : wordWithNgrams.getNgramsList()) {
                        // Find and insert ngram into dbTables or increment ngram counter
                        Ngram ngram = ngramsDao.findByDataString(currentNgramData);
                        if (ngram == null) {
                            int newNgramDbIndex = ngramsDao.insertNewNgram(currentNgramData);
                            ngram = new Ngram(newNgramDbIndex, currentNgramData, 1);
                        } else {
                            ngram.incrementCounter();
                            ngramsDao.updateNgramCounter(ngram);
                        }

                        // Find and insert ngram_to_word relation of increment ngram in word counter
                        NgramToWord ngramToWord = ngramsToWordsDao.findNgramToWord(ngram.getId(), wordDbId);
                        if (ngramToWord == null) {
                            ngramsToWordsDao.insertNewNgramsToWords(ngram.getId(), wordDbId);
                        } else {
                            if (isWordNew) {
                                ngramToWord.incrementCounter();
                                ngramsToWordsDao.updateNgramToWordCounter(ngramToWord);
                            }
                        }
                    }
                    connection.commit();
                }
            }
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            System.out.println(e.getMessage());
        } catch (IllegalInputSizeException e) {
            connection.rollback();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void closeDatabaseConnection() throws SQLException {
        connection.close();
    }

    protected void finalize() throws Throwable {
        try {
            closeDatabaseConnection();
        } finally {
            super.finalize();
        }
    }
}
