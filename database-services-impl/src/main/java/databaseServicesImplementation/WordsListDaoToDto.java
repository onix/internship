package databaseServicesImplementation;

import dao.entities.WordForNgramWithCount;
import databaseServicesApi.WordForNgramWithCountDto;

import java.util.ArrayList;
import java.util.List;

public class WordsListDaoToDto {
    public static List<WordForNgramWithCountDto> convert(List<WordForNgramWithCount> listToConvert) {
        List<WordForNgramWithCountDto> listToReturn = new ArrayList<WordForNgramWithCountDto>(listToConvert.size());

        for (WordForNgramWithCount element : listToConvert) {
            listToReturn.add(new WordForNgramWithCountDto(element.getWord(), element.getCount()));
        }

        return listToReturn;
    }
}
