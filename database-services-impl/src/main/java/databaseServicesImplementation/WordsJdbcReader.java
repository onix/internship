package databaseServicesImplementation;

import dao.NgramsDao;
import dao.WordsDao;
import dao.daoQueryObjects.WordsListDaoQueryObject;
import dao.jdbcDao.JdbcNgramsDao;
import dao.jdbcDao.JdbcWordsDao;
import databaseServicesApi.WordForNgramWithCountDto;
import databaseServicesApi.WordsDatabaseReaderInterface;
import databaseServicesApi.WordsListQueryObject;
import jdbcUtilities.JdbcH2DataSource;
import org.apache.commons.lang3.StringUtils;

import java.sql.Connection;
import java.util.List;

public class WordsJdbcReader implements WordsDatabaseReaderInterface {
    private final WordsDao wordsDao;
    private final NgramsDao ngramsDao;

    public WordsJdbcReader() {
        Connection connection = JdbcH2DataSource.getDBConnection();

        wordsDao = new JdbcWordsDao(connection);
        ngramsDao = new JdbcNgramsDao(connection);
    }

    @Override
    public List<WordForNgramWithCountDto> getListOfWordsWithCounters(WordsListQueryObject wordsListQuery) {
        if (wordsListQuery == null)
            return null;

        int ngramId = ngramsDao.getIdForNgram(wordsListQuery.getNgramData());
        WordsListDaoQueryObject wrdsDaoQO = WordsListQueryObjectToDaoQueryObjectConverter.convert(ngramId, wordsListQuery);
        return WordsListDaoToDto.convert(wordsDao.findAndExtractListOfWordsForNgramByPartialWordMatch(wrdsDaoQO));
    }

    @Override
    public int getAmountOfWordsWithCountersInDatabase(String ngramData, String partOfWord) {
        if (StringUtils.isEmpty(ngramData) || StringUtils.isEmpty(partOfWord))
            return 0;

        int ngramId = ngramsDao.getIdForNgram(ngramData);
        if (ngramId > 0) {
            return wordsDao.extractAmountOfWordsForNgramByItsDataPartialMatch(ngramId, partOfWord);
        }
        return 0;
    }
}
