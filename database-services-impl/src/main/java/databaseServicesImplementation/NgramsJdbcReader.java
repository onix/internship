package databaseServicesImplementation;

import dao.NgramsDao;
import dao.daoQueryObjects.NgramsListDaoQueryObject;
import dao.entities.Ngram;
import dao.jdbcDao.JdbcNgramsDao;
import databaseServicesApi.NgramDto;
import databaseServicesApi.NgramsDatabaseReaderInterface;
import databaseServicesApi.NgramsListQueryObject;
import jdbcUtilities.JdbcH2DataSource;

import java.util.List;

public class NgramsJdbcReader implements NgramsDatabaseReaderInterface {
    private final NgramsDao ngramsDao;

    public NgramsJdbcReader() {
        ngramsDao = new JdbcNgramsDao(JdbcH2DataSource.getDBConnection());
    }

    @Override
    public NgramDto searchNgramByDataFullMatch(String ngramData) {
        Ngram ngram = ngramsDao.findByDataString(ngramData);
        return new NgramDto(ngram.getData(), ngram.getCount());
    }

    @Override
    public List<NgramDto> getListOfNgrams(NgramsListQueryObject ngramsListQuery) {
        if (ngramsListQuery == null)
            return null;

        NgramsListDaoQueryObject ngramsDaoQO = NgramsListQueryObjectToDaoQueryObjectConverter.convert(ngramsListQuery);
        return NgramsListDaoToDto.convert(ngramsDao.extractListOfNgrams(ngramsDaoQO));
    }

    @Override
    public int getAmountOfNgramsInDatabase() {
        return ngramsDao.extractAmountOfNgrams();
    }
}
