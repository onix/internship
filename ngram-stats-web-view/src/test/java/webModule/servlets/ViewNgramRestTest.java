package webModule.servlets;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import webModule.WrongURIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ViewNgramRestTest {
    private static final List<String> CORRECT_URLS = new ArrayList<String>() {{
        add("http://example.com/ngram/ad");
        add("http://example.com/ngram/ad/");
        add("http://example.com/ngram/ad/12");
        add("http://example.com/ngram/ad/12/");
        add("http://example.com/ngram/ad/12/word");
        add("http://example.com/ngram/ad/12/word/");
        add("http://example.com/ngram/ad/12/word/asc");
        add("http://example.com/ngram/ad/12/word/asc/");
        add("http://example.com/ngram/ad/12/word/desc");
        add("http://example.com/ngram/ad/12/word/desc/");
        add("http://example.com/ngram/ad/12/count");
        add("http://example.com/ngram/ad/12/count/");
        add("http://example.com/ngram/ad/12/count/asc");
        add("http://example.com/ngram/ad/12/count/asc/");
        add("http://example.com/ngram/ad/12/count/desc");
        add("http://example.com/ngram/ad/12/count/desc/");
    }};

    private static final List<ViewNgramRest> CORRECT_URLS_PARSE_OBJECT = new ArrayList<ViewNgramRest>() {{
        add(new ViewNgramRest("ad", 1, null, null));
        add(new ViewNgramRest("ad", 1, null, null));
        add(new ViewNgramRest("ad", 12, null, null));
        add(new ViewNgramRest("ad", 12, null, null));
        add(new ViewNgramRest("ad", 12, "word", null));
        add(new ViewNgramRest("ad", 12, "word", null));
        add(new ViewNgramRest("ad", 12, "word", "asc"));
        add(new ViewNgramRest("ad", 12, "word", "asc"));
        add(new ViewNgramRest("ad", 12, "word", "desc"));
        add(new ViewNgramRest("ad", 12, "word", "desc"));
        add(new ViewNgramRest("ad", 12, "count", null));
        add(new ViewNgramRest("ad", 12, "count", null));
        add(new ViewNgramRest("ad", 12, "count", "asc"));
        add(new ViewNgramRest("ad", 12, "count", "asc"));
        add(new ViewNgramRest("ad", 12, "count", "desc"));
        add(new ViewNgramRest("ad", 12, "count", "desc"));
    }};

    @DataProvider(name = "wrongUrls")
    public Object[][] createData() throws IOException {
        return new Object[][]{
                {"http://example.com/ngrams/viea"},
                {"http://example.com/ngrams/view/f"},
                {"http://example.com/ngrams/view/78541a"},
                {"http://example.com/ngrams/view/7854112355125623452235/"},
                {"http://example.com/ngrams/view/78541/nama"},
                {"http://example.com/ngrams/view/78541/name/f"},
                {"http://example.com/ngrams/view/78541/name/asa"},
                {"http://example.com/ngrams/view/78541/naame/desc"},
                {"http://example.com/ngrams/view/78541/caunt"},
                {"http://example.com/ngrams/view/78541/counti/"},
                {"http://example.com/ngrams/view/78541/cu3nt/asc"},
                {"http://example.com/ngrams/view/78541/counta/asc/"},
                {"http://example.com/ngrams/view/78541/countg/desc"},
                {"http://example.com/ngrams/view/78541a/count/desc/"},
        };
    }

    @Test
    public void testCorrectUrlParse() throws Exception {
        for (int i = 0; i < CORRECT_URLS.size(); i++) {
            Assert.assertEquals(new ViewNgramRest(CORRECT_URLS.get(i)), CORRECT_URLS_PARSE_OBJECT.get(i));
        }
    }

    @Test(dataProvider = "wrongUrls", expectedExceptions = WrongURIException.class)
    public void testThatFailsOnWrongUrls(String url) throws Exception {
        new ViewNgramsRest(url);
    }
}
