package webModule.servlets;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import webModule.WrongURIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NgramsViewRestTest {
    private static final List<String> CORRECT_URLS = new ArrayList<String>() {{
        add("http://example.com/ngrams/view");
        add("http://example.com/ngrams/view/");
        add("http://example.com/ngrams/view/78541");
        add("http://example.com/ngrams/view/78541/");
        add("http://example.com/ngrams/view/78541/name");
        add("http://example.com/ngrams/view/78541/name/");
        add("http://example.com/ngrams/view/78541/name/asc");
        add("http://example.com/ngrams/view/78541/name/asc/");
        add("http://example.com/ngrams/view/78541/name/desc");
        add("http://example.com/ngrams/view/78541/name/desc/");
        add("http://example.com/ngrams/view/78541/count");
        add("http://example.com/ngrams/view/78541/count/");
        add("http://example.com/ngrams/view/78541/count/asc");
        add("http://example.com/ngrams/view/78541/count/asc/");
        add("http://example.com/ngrams/view/78541/count/desc");
        add("http://example.com/ngrams/view/78541/count/desc/");
    }};

    private static final List<ViewNgramsRest> CORRECT_URLS_PARSE_OBJECT = new ArrayList<ViewNgramsRest>() {{
        add(new ViewNgramsRest(1, null, null));
        add(new ViewNgramsRest(1, null, null));
        add(new ViewNgramsRest(78541, null, null));
        add(new ViewNgramsRest(78541, null, null));
        add(new ViewNgramsRest(78541, "name", null));
        add(new ViewNgramsRest(78541, "name", null));
        add(new ViewNgramsRest(78541, "name", "asc"));
        add(new ViewNgramsRest(78541, "name", "asc"));
        add(new ViewNgramsRest(78541, "name", "desc"));
        add(new ViewNgramsRest(78541, "name", "desc"));
        add(new ViewNgramsRest(78541, "count", null));
        add(new ViewNgramsRest(78541, "count", null));
        add(new ViewNgramsRest(78541, "count", "asc"));
        add(new ViewNgramsRest(78541, "count", "asc"));
        add(new ViewNgramsRest(78541, "count", "desc"));
        add(new ViewNgramsRest(78541, "count", "desc"));
    }};

    @DataProvider(name = "wrongUrls")
    public Object[][] createData() throws IOException {
        return new Object[][]{
                {"http://example.com/ngrams/viea"},
                {"http://example.com/ngrams/view/f"},
                {"http://example.com/ngrams/view/78541a"},
                {"http://example.com/ngrams/view/7854112355125623452235/"},
                {"http://example.com/ngrams/view/78541/nama"},
                {"http://example.com/ngrams/view/78541/name/f"},
                {"http://example.com/ngrams/view/78541/name/asa"},
                {"http://example.com/ngrams/view/78541/naame/desc"},
                {"http://example.com/ngrams/view/78541/caunt"},
                {"http://example.com/ngrams/view/78541/counti/"},
                {"http://example.com/ngrams/view/78541/c3unt/asc"},
                {"http://example.com/ngrams/view/78541/counta/asc/"},
                {"http://example.com/ngrams/view/78541/countg/desc"},
                {"http://example.com/ngrams/view/78541a/count/desc/"},
        };
    }

    @Test
    public void testCorrectUrlParse() throws Exception {
        for (int i = 0; i < CORRECT_URLS.size(); i++) {
            Assert.assertEquals(new ViewNgramsRest(CORRECT_URLS.get(i)), CORRECT_URLS_PARSE_OBJECT.get(i));
        }
    }

    @Test(dataProvider = "wrongUrls", expectedExceptions = WrongURIException.class)
    public void testThatFailsOnWrongUrls(String url) throws Exception {
        new ViewNgramsRest(url);
    }
}
