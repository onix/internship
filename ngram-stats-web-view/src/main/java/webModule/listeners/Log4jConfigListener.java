package webModule.listeners;

import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class Log4jConfigListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent event) {
        // initialize log4j here
        ServletContext context = event.getServletContext();
        String log4jConfigFile = context.getInitParameter("log4j-config-location");
        PropertyConfigurator.configure(Log4jConfigListener.class.getResource(log4jConfigFile));
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
    }
}
