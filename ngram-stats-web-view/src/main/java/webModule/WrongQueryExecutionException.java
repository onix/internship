package webModule;

public class WrongQueryExecutionException extends Exception {
    public WrongQueryExecutionException(String message) {
        super(message);
    }
}
