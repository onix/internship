package webModule;

public class WrongURIException extends Exception {
    public WrongURIException(String wrongURI) {
        super(wrongURI);
    }
}
