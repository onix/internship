package webModule.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HomepageServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomepageServlet.class);

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("Redirecting to the ngrams view page.");
        response.sendRedirect(getServletContext().getContextPath() + "/ngrams/view");
    }
}
