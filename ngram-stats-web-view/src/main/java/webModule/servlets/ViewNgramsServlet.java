package webModule.servlets;

import databaseServicesApi.*;
import databaseServicesImplementation.NgramsJdbcReader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webModule.WrongQueryExecutionException;
import webModule.WrongURIException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewNgramsServlet extends HttpServlet {
    private static final String JSP_VIEW_TABLE_OF_NGRAMS_URL = "/WEB-INF/pages/ngrams/view.jsp";
    private static final String JSP_VIEW_SEARCH_RESULT = "/WEB-INF/pages/ngrams/search-result.jsp";
    private static final String COLLECTION_URL_SERVLET_IS_MAPPED = "/ngrams/view";
    private static final int AMOUNT_OF_ELEMENTS_PER_PAGE = 10;
    private static final Logger logger = LoggerFactory.getLogger(ViewNgramsServlet.class);

    private static final String SEARCH_PLACEHOLDER = "Search for ngrams (full match)";

    private RequestDispatcher returnSearchResultPage(HttpServletRequest request, String searchNgramData) {
        logger.debug("User input is search query " + searchNgramData);
        NgramsDatabaseReaderInterface dbreader = new NgramsJdbcReader();
        final NgramDto ngram = dbreader.searchNgramByDataFullMatch(searchNgramData);

        request.setAttribute("searchResult", new HashMap<String, NgramDto>() {{
            put("ngram", ngram);
        }});

        Map<String, String> searchInfo = new HashMap<String, String>() {{
            put("searchPlaceholder", SEARCH_PLACEHOLDER);
        }};

        if (StringUtils.isNotEmpty(searchNgramData)) {
            searchInfo.put("searchValue", searchNgramData);
        }
        request.setAttribute("searchInfo", searchInfo);

        return request.getRequestDispatcher(JSP_VIEW_SEARCH_RESULT);
    }

    private RequestDispatcher returnListOfNgramsPage(HttpServletRequest request) throws WrongURIException, WrongQueryExecutionException {
        logger.debug("Show a list of ngrams page.");
        NgramsDatabaseReaderInterface dbreader = new NgramsJdbcReader();
        int pageId; // = 1 is minimum index at current stage. It's important, that database extracts pages from 0-index
        List<NgramDto> listOfNgrams;
        String pathConditionsPart = "";

        final ViewNgramsRest restParser = new ViewNgramsRest(request.getRequestURI());
        pageId = restParser.getPageId();

        NgramsSortFieldName orderFieldName;
        Order order;

        try {
            orderFieldName = NgramsSortFieldName.valueOf(restParser.getOrderByFieldName());
        } catch (IllegalArgumentException e) {
            orderFieldName = null;
        } catch (Exception e) {
            orderFieldName = null;
        }

        try {
            order = Order.valueOf(restParser.getOrder());
        } catch (IllegalArgumentException e) {
            order = null;
        } catch (Exception e) {
            order = null;
        }

        int amountOfPages = (int) Math.ceil((double) dbreader.getAmountOfNgramsInDatabase() / AMOUNT_OF_ELEMENTS_PER_PAGE);
        // In the case if input page is wrong or even negative, first page id will be applied by Rest Parser
        if (pageId > amountOfPages) {
            pageId = amountOfPages;
        }

        try {
            listOfNgrams = dbreader.getListOfNgrams(
                    new NgramsListQueryObject.NgramsListQueryBuilder(AMOUNT_OF_ELEMENTS_PER_PAGE, pageId - 1)
                            .orderByField(orderFieldName)
                            .order(order).build()
            );
        } catch (QueryObjectBuilderValidationError e) {
            try {
                listOfNgrams = dbreader.getListOfNgrams(
                        new NgramsListQueryObject.NgramsListQueryBuilder(AMOUNT_OF_ELEMENTS_PER_PAGE, 0).build()
                );
            } catch (QueryObjectBuilderValidationError queryObjectBuilderValidationError) {
                throw new WrongQueryExecutionException("Even with simplified parameters, execution of ngrams list extraction operation is impossible.");
            }
        }

        final int finalPageId = pageId;
        request.setAttribute("sortInfo", new HashMap<String, String>() {{
            put("URISortMoulderCurrentCollectionPath", COLLECTION_URL_SERVLET_IS_MAPPED);
            put("URISortMoulderCurrentPageNumber", String.valueOf(finalPageId));
            put("URISortMoulderFieldToSort", restParser.getOrderByFieldName());
            put("URISortMoulderOrder", restParser.getOrder());
        }});

        request.setAttribute("paginatorAmountOfPages", amountOfPages);
        request.setAttribute("paginatorCurrentPage", pageId);
        request.setAttribute("paginatorRestCollectionPath", COLLECTION_URL_SERVLET_IS_MAPPED);
        request.setAttribute("paginatorRestQueryParameters", pathConditionsPart);

        final List<NgramDto> finalListOfNgrams = listOfNgrams;
        request.setAttribute("listResult", new HashMap<String, List>() {{
            put("listOfNgrams", finalListOfNgrams);
        }});

        request.setAttribute("searchInfo", new HashMap<String, String>() {{
            put("searchPlaceholder", SEARCH_PLACEHOLDER);
        }});

        return request.getRequestDispatcher(JSP_VIEW_TABLE_OF_NGRAMS_URL);
    }

    /**
     * This doGet handles search queries to full match of ngram (that is determined as 'query' parameter: ngrams/view?query= ), and REST GET URLs:
     * ngrams/view/[[pageNumber]/[name|count/[asc|desc]]]
     *
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ngramSearchData = request.getParameter("query");

        RequestDispatcher view;
        try {
            if (StringUtils.isEmpty(ngramSearchData)) {
                view = returnListOfNgramsPage(request);
            } else {
                view = returnSearchResultPage(request, ngramSearchData);
            }

            if (view != null) {
                view.forward(request, response);
            }
        } catch (WrongURIException e) {
            logger.debug("WrongURIException catched");
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } catch (Exception e) {
            logger.debug("Exception catched", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}