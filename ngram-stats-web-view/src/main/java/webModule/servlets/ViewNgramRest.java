package webModule.servlets;

import webModule.WrongURIException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates such kinds of URLs (and parses them):
 * <p/>
 * http://example.com/ngram/ad
 * http://example.com/ngram/ad/
 * http://example.com/ngram/ad/12
 * http://example.com/ngram/ad/12/
 * http://example.com/ngram/ad/12/word
 * http://example.com/ngram/ad/12/word/
 * http://example.com/ngram/ad/12/word/asc
 * http://example.com/ngram/ad/12/word/asc/
 * http://example.com/ngram/ad/12/word/desc
 * http://example.com/ngram/ad/12/word/desc/
 * http://example.com/ngram/ad/12/count
 * http://example.com/ngram/ad/12/count/
 * http://example.com/ngram/ad/12/count/asc
 * http://example.com/ngram/ad/12/count/asc/
 * http://example.com/ngram/ad/12/count/desc
 * http://example.com/ngram/ad/12/count/desc/
 */

public class ViewNgramRest {
    private static final Pattern REGEX_ALL_PATTERN = Pattern.compile("^.{0,}/ngram");
    // ^.{0,}/ngram(?:/(?:([^/]{1,20})(?:/(?:([0-9]{1,10})(?:/(?:(word|count)(?:/(?:(asc|desc)(?:/)?)?)?)?)?)?)?)?)?$
    private static final Pattern REGEX_ORDER_PATTERN = Pattern.compile("^.{0,}/ngram" +
            "(?:" +
            "/" +
            "(?:" +
            "([^/]{1,20})" +
            "(?:" +
            "/" +
            "(?:" +
            "([0-9]{1,10})" +
            "(?:" +
            "/" +
            "(?:" +
            "(word|count)" +
            "(?:" +
            "/" +
            "(?:" +
            "(asc|desc)" +
            "(?:/)?" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            "$");

    private final String ngramData;
    private final int pageId;
    private final String orderByFieldName; // 'word' or 'count'
    private final String order;

    public ViewNgramRest(String ngramData, int pageId, String orderByFieldName, String order) {
        this.ngramData = ngramData;
        this.pageId = pageId;
        this.orderByFieldName = orderByFieldName;
        this.order = order;
    }

    public ViewNgramRest(String requestURI) throws WrongURIException {
        if (requestURI == null)
            throw new IllegalArgumentException("Path Info is null.");

        Matcher matcher;

        matcher = REGEX_ALL_PATTERN.matcher(requestURI);
        if (matcher.find()) {

            matcher = REGEX_ORDER_PATTERN.matcher(requestURI);
            if (matcher.find()) {
                int pageIdMutable;
                ngramData = matcher.group(1);
                if (ngramData == null) {
                    throw new WrongURIException("Wrong URI. Ngram not specified.");
                }

                try {
                    pageIdMutable = Integer.parseInt(matcher.group(2));
                    if (pageIdMutable == 0) {
                        pageIdMutable = 1;
                    }
                } catch (NumberFormatException e) {
                    pageIdMutable = 1;
                }
                pageId = pageIdMutable;
                orderByFieldName = matcher.group(3);
                order = matcher.group(4);
            } else {
                throw new WrongURIException("Wrong URI.");
            }
        } else {
            throw new WrongURIException("Wrong URI.");
        }
    }

    public String getNgramData() {
        return ngramData;
    }

    public int getPageId() {
        return pageId;
    }

    public String getOrderByFieldName() {
        return orderByFieldName;
    }

    public String getOrder() {
        return order;
    }

    // Used for test purposes
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ViewNgramRest that = (ViewNgramRest) o;

        if (pageId != that.pageId) return false;
        if (ngramData != null ? !ngramData.equals(that.ngramData) : that.ngramData != null) return false;
        if (order != null ? !order.equals(that.order) : that.order != null) return false;
        if (orderByFieldName != null ? !orderByFieldName.equals(that.orderByFieldName) : that.orderByFieldName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ngramData != null ? ngramData.hashCode() : 0;
        result = 31 * result + pageId;
        result = 31 * result + (orderByFieldName != null ? orderByFieldName.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        return result;
    }
}
