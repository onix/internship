package webModule.servlets;

import dao.daoQueryObjects.Order;
import databaseServicesApi.*;
import databaseServicesImplementation.WordsJdbcReader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webModule.WrongQueryExecutionException;
import webModule.WrongURIException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class ViewNgramServlet extends HttpServlet {
    private static final String JSP_VIEW_NGRAM_URL = "/WEB-INF/pages/ngram/view.jsp";
    private static final String JSP_VIEW_SEARCH_RESULT = "/WEB-INF/pages/ngram/search-result.jsp";
    private static final String COLLECTION_URL_SERVLET_IS_MAPPED = "/ngram";
    private static final String REGEX_PATTERN_TO_EXTRACT_ALL_WORDS = ".";
    private static final int AMOUNT_OF_ELEMENTS_PER_PAGE = 10;
    private static final Logger logger = LoggerFactory.getLogger(ViewNgramServlet.class);

    private static final String SEARCH_PLACEHOLDER = "Search among all words for this ngram (partial/regex)";

    private void GetItemsTable(HttpServletRequest request, final String patternToFilter, final ViewNgramRest restParser, final boolean isSearchResult) throws WrongQueryExecutionException {
        int pageId;
        WordsDatabaseReaderInterface dbreader = new WordsJdbcReader();
        List<WordForNgramWithCountDto> listOfWordsForNgram;
        String pathPostConditions = "";

        final String ngramData = restParser.getNgramData();
        pageId = restParser.getPageId();

        WordsSortFieldName orderFieldName;
        Order order;

        try {
            orderFieldName = WordsSortFieldName.valueOf(restParser.getOrderByFieldName());
        } catch (IllegalArgumentException e) {
            orderFieldName = null;
        } catch (Exception e) {
            orderFieldName = null;
        }

        try {
            order = Order.valueOf(restParser.getOrder());
        } catch (IllegalArgumentException e) {
            order = null;
        } catch (Exception e) {
            order = null;
        }

        int amountOfPages = (int) Math.ceil((double) dbreader.getAmountOfWordsWithCountersInDatabase(ngramData, patternToFilter) / AMOUNT_OF_ELEMENTS_PER_PAGE);
        // In the case if input page is wrong or even negative, first page id will be applied by Rest Parser
        if (pageId > amountOfPages) {
            pageId = amountOfPages;
        }

        try {
            listOfWordsForNgram = dbreader.getListOfWordsWithCounters(
                    new WordsListQueryObject.WordsListQueryBuilder(ngramData, AMOUNT_OF_ELEMENTS_PER_PAGE, pageId - 1, patternToFilter)
                            .orderByField(orderFieldName)
                            .order(order).build()
            );

        } catch (QueryObjectBuilderValidationError e) {
            try {
                listOfWordsForNgram = dbreader.getListOfWordsWithCounters(
                        new WordsListQueryObject.WordsListQueryBuilder(ngramData, AMOUNT_OF_ELEMENTS_PER_PAGE, 0, REGEX_PATTERN_TO_EXTRACT_ALL_WORDS).build()
                );
            } catch (QueryObjectBuilderValidationError queryObjectBuilderValidationError) {
                throw new WrongQueryExecutionException("Even with simplified parameters, execution of words list extraction operation is impossible.");
            }
        }

        final int finalPageId = pageId;
        request.setAttribute("sortInfo", new HashMap<String, String>() {{
            put("URISortMoulderCurrentCollectionPath", COLLECTION_URL_SERVLET_IS_MAPPED + "/" + restParser.getNgramData());
            put("URISortMoulderCurrentPageNumber", String.valueOf(finalPageId));
            put("URISortMoulderFieldToSort", restParser.getOrderByFieldName());
            put("URISortMoulderOrder", restParser.getOrder());
            if (isSearchResult) {
                put("URISortMoulderAdditionalParameters", "?query=" + patternToFilter);
            }
        }});

        request.setAttribute("paginatorAmountOfPages", amountOfPages);
        request.setAttribute("paginatorCurrentPage", finalPageId);
        request.setAttribute("paginatorRestCollectionPath", COLLECTION_URL_SERVLET_IS_MAPPED + "/" + ngramData);
        if (isSearchResult) {
            request.setAttribute("paginatorRestQueryParameters", pathPostConditions + "?query=" + patternToFilter);
        } else {
            request.setAttribute("paginatorRestQueryParameters", pathPostConditions);
        }

        request.setAttribute("searchInfo", new HashMap<String, String>() {{
            put("searchPlaceholder", SEARCH_PLACEHOLDER);
            if (!patternToFilter.equals(".")) {
                put("searchValue", patternToFilter);
            }
            put("searchAction", "/ngram/" + ngramData + "/");
        }});

        final List<WordForNgramWithCountDto> finalListOfWordsForNgram = listOfWordsForNgram;
        request.setAttribute("listResult", new HashMap<String, List>() {{
            put("listOfWordsForNgram", finalListOfWordsForNgram);
        }});
        request.setAttribute("ngramDataString", ngramData);
    }

    private RequestDispatcher returnSearchOfWordsForThisNgramResultPage(HttpServletRequest request, final String wordSearchData, final ViewNgramRest restParser) throws WrongQueryExecutionException {
        logger.debug("Showing a search result page.");
        GetItemsTable(request, wordSearchData, restParser, true);

        request.setAttribute("searchWordPatternDataString", wordSearchData);

        return request.getRequestDispatcher(JSP_VIEW_SEARCH_RESULT);
    }

    private RequestDispatcher returnListOfWordsForThisNgramPage(HttpServletRequest request, final String patternToFilter, final ViewNgramRest restParser) throws WrongQueryExecutionException {
        logger.debug("Showing an an ngram page (words for ngram and their amount).");
        GetItemsTable(request, patternToFilter, restParser, false);

        return request.getRequestDispatcher(JSP_VIEW_NGRAM_URL);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String wordSearchData = request.getParameter("query");
        RequestDispatcher view;
        try {
            final ViewNgramRest restParser = new ViewNgramRest(request.getRequestURI());

            if (StringUtils.isEmpty(wordSearchData)) {
                view = returnListOfWordsForThisNgramPage(request, REGEX_PATTERN_TO_EXTRACT_ALL_WORDS, restParser);
            } else {
                view = returnSearchOfWordsForThisNgramResultPage(request, wordSearchData, restParser);
            }

            if (view != null) {
                view.forward(request, response);
            }
        } catch (WrongURIException e) {
            logger.debug("WrongURIException catched");
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } catch (Exception e) {
            logger.debug("Exception catched", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
