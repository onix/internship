package webModule.servlets;

import webModule.WrongURIException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates such kinds of URLs (and parses them):
 * <p/>
 * http://example.com/ngrams/view
 * http://example.com/ngrams/view/
 * http://example.com/ngrams/view/
 * http://example.com/ngrams/view/78541
 * http://example.com/ngrams/view/78541/name
 * http://example.com/ngrams/view/78541/name/
 * http://example.com/ngrams/view/78541/name/asc
 * http://example.com/ngrams/view/78541/name/asc/
 * http://example.com/ngrams/view/78541/name/desc
 * http://example.com/ngrams/view/78541/name/desc/
 * http://example.com/ngrams/view/78541/count
 * http://example.com/ngrams/view/78541/count/
 * http://example.com/ngrams/view/78541/count/asc
 * http://example.com/ngrams/view/78541/count/asc/
 * http://example.com/ngrams/view/78541/count/desc
 * http://example.com/ngrams/view/78541/count/desc/
 */

public class ViewNgramsRest {
    private static final Pattern REGEX_ALL_PATTERN = Pattern.compile("^.{0,}/ngrams/view");
    // ^.{0,}/ngrams/view(?:/(?:([0-9]{1,10})(?:/(?:(name|count)(?:/(?:(asc|desc)(?:/)?)?)?)?)?)?)?$
    private static final Pattern REGEX_ORDER_PATTERN = Pattern.compile("^.{0,}/ngrams/view" +
            "(?:" +
            "/" +
            "(?:" +
            "([0-9]{1,10})" +
            "(?:" +
            "/" +
            "(?:" +
            "(name|count)" +
            "(?:" +
            "/" +
            "(?:" +
            "(asc|desc)" +
            "(?:" +
            "/" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            ")?" +
            "$");

    private final int pageId;
    private final String orderByFieldName;
    private final String order;

    public ViewNgramsRest(int pageId, String orderByFieldName, String order) {
        this.pageId = pageId;
        this.orderByFieldName = orderByFieldName;
        this.order = order;
    }

    public ViewNgramsRest(String requestURI) throws WrongURIException {
        if (requestURI == null)
            throw new IllegalArgumentException("Path Info is null.");

        Matcher matcher;

        matcher = REGEX_ALL_PATTERN.matcher(requestURI);
        if (matcher.find()) {

            matcher = REGEX_ORDER_PATTERN.matcher(requestURI);
            if (matcher.find()) {
                int pageIdMutable;
                try {
                    pageIdMutable = Integer.parseInt(matcher.group(1));
                    if (pageIdMutable == 0) {
                        pageIdMutable = 1;
                    }
                } catch (NumberFormatException e) {
                    pageIdMutable = 1;
                }
                pageId = pageIdMutable;
                orderByFieldName = matcher.group(2);
                order = matcher.group(3);
            } else {
                throw new WrongURIException("Wrong URI.");
            }
        } else {
            throw new WrongURIException("Wrong URI.");
        }
    }

    public int getPageId() {
        return pageId;
    }

    public String getOrderByFieldName() {
        return orderByFieldName;
    }

    public String getOrder() {
        return order;
    }

    // Used for test purposes
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ViewNgramsRest that = (ViewNgramsRest) o;

        if (pageId != that.pageId) return false;
        if (order != null ? !order.equals(that.order) : that.order != null) return false;
        if (orderByFieldName != null ? !orderByFieldName.equals(that.orderByFieldName) : that.orderByFieldName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pageId;
        result = 31 * result + (orderByFieldName != null ? orderByFieldName.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        return result;
    }
}