<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pagination" uri="/WEB-INF/tags/pagination-tags.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%--
    The universal paginator component. To use this component you need to include it into page and pass through the model:
    -- current page number (for 1 to n)
    -- general (total) amount of pages (index is in diapason for 1 to n)
    -- links path divided into two parts (http://example.com/hello/world/example):
    ---- rest colelction path ("hello/world")
    ---- rest query parameters path ("example")
--%>

<jsp:useBean id="paginatorCurrentPage" scope="request" type="java.lang.Integer"/>
<jsp:useBean id="paginatorAmountOfPages" scope="request" type="java.lang.Integer"/>
<jsp:useBean id="paginatorRestCollectionPath" scope="request" type="java.lang.String"/>
<jsp:useBean id="paginatorRestQueryParameters" scope="request" type="java.lang.String"/>

<c:if test="${paginatorAmountOfPages > 1}">
    <div class="pagination pagination-mini pagination-centered">
        <ul>
                <%--Display first page button--%>
            <c:choose>
                <c:when test="${paginatorCurrentPage == 1}">
                    <li class="disabled">
                        <a href="#"> First </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a href="<c:url value="${paginatorRestCollectionPath}/1/${paginatorRestQueryParameters}"/>"> First </a>
                    </li>
                </c:otherwise>
            </c:choose>

                <%-- Display Previous link except for the 1st page --%>
            <c:if test="${paginatorCurrentPage != 1}">
                <li>
                    <a href="<c:url value="${paginatorRestCollectionPath}/${paginatorCurrentPage - 1}/${paginatorRestQueryParameters}"/>">
                        Previous
                    </a>
                </li>
            </c:if>

                <%-- Display Page numbers. The when condition does not display a link for the current page --%>
                <%-- TODO Try to optimize? --%>
            <c:choose>
                <%-- Render this when page is 1..3 --%>
                <c:when test="${(paginatorCurrentPage <= 3)}">
                    <c:choose>
                        <c:when test="${paginatorAmountOfPages > 3}">
                            <c:forEach begin="1" end="${paginatorCurrentPage + 2}" var="p">
                                <c:choose>
                                    <c:when test="${paginatorCurrentPage == p}">
                                        <pagination:active-page current="${p}" total="${paginatorAmountOfPages}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <pagination:non-active-page paginatorPathPrePart="${paginatorRestCollectionPath}"
                                                                    paginatorPathPostPart="${paginatorRestQueryParameters}"
                                                                    p="${p}"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            <li class="disabled"><a href="#">...</a></li>
                        </c:when>

                        <%-- If we have only two or three pages --%>
                        <c:otherwise>
                            <c:forEach begin="1" end="${paginatorAmountOfPages}" var="p">
                                <c:choose>
                                    <c:when test="${paginatorCurrentPage == p}">
                                        <pagination:active-page current="${p}" total="${paginatorAmountOfPages}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <pagination:non-active-page paginatorPathPrePart="${paginatorRestCollectionPath}"
                                                                    paginatorPathPostPart="${paginatorRestQueryParameters}"
                                                                    p="${p}"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </c:when>

                <%-- 4..n-3 --%>
                <c:when test="${(paginatorCurrentPage > 3) && (paginatorCurrentPage <= paginatorAmountOfPages - 3)}">
                    <li class="disabled"><a href="#">...</a></li>
                    <c:forEach begin="${paginatorCurrentPage - 2}" end="${paginatorCurrentPage + 2}" var="p">
                        <c:choose>
                            <c:when test="${paginatorCurrentPage == p}">
                                <pagination:active-page current="${p}" total="${paginatorAmountOfPages}"/>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <pagination:non-active-page paginatorPathPrePart="${paginatorRestCollectionPath}"
                                                                paginatorPathPostPart="${paginatorRestQueryParameters}"
                                                                p="${p}"/>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <li class="disabled"><a href="#">...</a></li>
                </c:when>

                <%-- n-2..n --%>
                <c:when test="${(paginatorCurrentPage >= paginatorAmountOfPages - 2)}">
                    <li class="disabled"><a href="#">...</a></li>
                    <c:forEach begin="${paginatorCurrentPage - 2}" end="${paginatorAmountOfPages}" var="p">
                        <c:choose>
                            <c:when test="${paginatorCurrentPage == p}">
                                <pagination:active-page current="${p}" total="${paginatorAmountOfPages}"/>
                            </c:when>
                            <c:otherwise>
                                <pagination:non-active-page paginatorPathPrePart="${paginatorRestCollectionPath}"
                                                            paginatorPathPostPart="${paginatorRestQueryParameters}"
                                                            p="${p}"/>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </c:when>
            </c:choose>

                <%-- Display Next link --%>
            <c:if test="${paginatorCurrentPage != paginatorAmountOfPages}">
                <li>
                    <a href="<c:url value="${paginatorRestCollectionPath}/${paginatorCurrentPage + 1}/${paginatorRestQueryParameters}"/>">
                        Next </a>
                </li>
            </c:if>

                <%--Display last page button--%>
            <c:choose>
                <c:when test="${paginatorCurrentPage == paginatorAmountOfPages}">
                    <li class="disabled"><a href="#"> Last </a></li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a href="<c:url value="${paginatorRestCollectionPath}/${paginatorAmountOfPages}/${paginatorRestQueryParameters}"/>">
                            Last </a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</c:if>