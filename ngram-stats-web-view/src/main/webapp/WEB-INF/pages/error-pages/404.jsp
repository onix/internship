<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="/WEB-INF/pages/includes/common-header.jspf" %>
    <title>404 – Not Found</title>
</head>
<body style="background-color: #2067B2">

<%@ include file="/WEB-INF/pages/includes/common-navbar.jspf" %>

<div class="container" id="container">
    <!-- This page has a lot of css code and is harmful for brain -->
    <h1 style="width: 500px; color: #FFFFFF;">404 – Not Found</h1>

    <p style="font-family:serif; color: #FFFFFF; font-size: 235pt;
        display: block; float: right; margin-top: 50px; margin-bottom: 150px;">:(</p>
    <br/>
    <br/>
    <br/>

    <h2 style="width: 500px; font-family: Comic Sans MS,serif; color: #FFFFFF;">The requested URL not found.</h2>
    <br/>
    <br/>

    <buttons:back-to-main-page-button/>
</div>

<%@ include file="/WEB-INF/pages/includes/common-footer.jspf" %>
</body>
</html>