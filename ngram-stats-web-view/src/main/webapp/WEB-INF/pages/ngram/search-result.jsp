<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>

<jsp:useBean id="sortInfo" scope="request" type="java.util.Map"/>

<%--<jsp:useBean id="listOfWordsForNgram" scope="request" type="java.util.List<dao.entities.logical.WordForNgramWithCountDto>"/>--%>

<jsp:useBean id="ngramDataString" scope="request" type="java.lang.String"/>
<jsp:useBean id="searchWordPatternDataString" scope="request" type="java.lang.String"/>

<%-- have listOfWordsForNgram inside: --%>
<jsp:useBean id="listResult" scope="request" type="java.util.Map<java.lang.String,java.util.List>"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="/WEB-INF/pages/includes/common-header.jspf" %>
    <title>View words for n-gram '${ngramDataString}'</title>
</head>
<body>

<%@ include file="/WEB-INF/pages/includes/common-navbar.jspf" %>

<div class="container" id="container">
    <div class="span6 center-element">
        <h1>Search result for n-gram: '<a href="<c:url value="/ngram/${ngramDataString}"/>">${ngramDataString}</a>'</h1>

        <h1>Word pattern: '${searchWordPatternDataString}'</h1>
        <br/>
        <%@ include file="/WEB-INF/pages/includes/search.jspf" %>
        <br/>

        <c:choose>
            <c:when test="${listResult.listOfWordsForNgram != null}">
                <table id="content-table" class="table table-bordered table-striped data-table sort display">
                    <thead>
                    <tr>
                        <th class="sorting">Words <buttons:table-sort-icon-generator sortInfo="${sortInfo}"
                                                                                     currentFieldName="word"/></th>
                        <th class="sorting">N-Grams count <buttons:table-sort-icon-generator sortInfo="${sortInfo}"
                                                                                             currentFieldName="count"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${listResult.listOfWordsForNgram}" var="word">
                        <tr>
                            <td>
                                <c:out value="${word.word}"/>
                            </td>
                            <td>
                                <c:out value="${word.count}"/>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br/>
                <%@ include file="/WEB-INF/pages/includes/pagination.jspf" %>
            </c:when>
            <c:otherwise>
                <h2>Nothing found</h2>

                <p>Seems, that we have no results for this query. Retry your search, please.</p>
                <br/>
                <br/>
                <buttons:back-to-main-page-button/>
            </c:otherwise>
        </c:choose>
    </div>

</div>

<%@ include file="/WEB-INF/pages/includes/common-footer.jspf" %>
</body>
</html>