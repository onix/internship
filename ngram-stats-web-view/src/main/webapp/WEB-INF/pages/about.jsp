<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="buttons" uri="/WEB-INF/tags/anchors-buttons.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page trimDirectiveWhitespaces="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="/WEB-INF/pages/includes/common-header.jspf" %>
    <title>About this viewer</title>
</head>
<body>

<%@ include file="/WEB-INF/pages/includes/common-navbar.jspf" %>

<div class="container" id="container">
    <div class="span6 center-element">

        <div class="text-center">
            <h1>N-grams statistic web viewer</h1>
            <p>By Vadim Ne. as a task of internship project for Grid Dynamics.</p>
            <p>Kharkiv, autumn-winter 2013</p>
            <br />
            <br />
            <br />
            <buttons:back-to-main-page-button/>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/pages/includes/common-footer.jspf" %>
</body>
</html>