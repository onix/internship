<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag trimDirectiveWhitespaces="true" %>

<%--This tag generates anchor (link) and an icon to refer to appropriate tabe sorting url.
    -- sortInfo bean refers to Map<String, String> which contains
        ---- URISortMoulderCurrentCollectionPath -- contain current path info without last slash (e.g. for http://example.com/one/two/, this value may be "/one/two")
        ---- URISortMoulderCurrentPageNumber -- page to get after the sort will be applied ${sortInfo.URISortMoulderCurrentPageNumber}
        ---- URISortMoulderFieldToSort -- name of the field to sort by
        ---- URISortMoulderOrder -- order of sort [asc|desc]
        ---- URISortMoulderAdditionalParameters -- parameters that will be added to the end of generated URL
    -- currentFieldName String that names the field where this tag inserted.
--%>

<%@ attribute name="sortInfo" required="true" type="java.util.Map" %>
<%@ attribute name="currentFieldName" required="true" type="java.lang.String" %>

<c:choose>
    <c:when test="${sortInfo.URISortMoulderFieldToSort == currentFieldName}">
        <c:if test="${sortInfo.URISortMoulderOrder == 'asc'}"> <%--From asc switch to default sort method.--%>
            <a href="<c:out value="${sortInfo.URISortMoulderCurrentCollectionPath}/1/${sortInfo.URISortMoulderAdditionalParameters}"/>">
                <i class="icon-chevron-up element-glow"></i>
            </a>
        </c:if>
        <c:if test="${sortInfo.URISortMoulderOrder == 'desc'}"> <%--From desc switch to asc sort method.--%>
            <a href="<c:out value="${sortInfo.URISortMoulderCurrentCollectionPath}/1/${currentFieldName}/asc/${sortInfo.URISortMoulderAdditionalParameters}"/>">
                <i class="icon-chevron-down element-glow"></i>
            </a>
        </c:if>
    </c:when>
    <c:otherwise> <%--From default switch to desc sort method.--%>
        <a href="<c:out value="${sortInfo.URISortMoulderCurrentCollectionPath}/1/${currentFieldName}/desc/${sortInfo.URISortMoulderAdditionalParameters}"/>">
            <i class="icon-list element-glow"></i>
        </a>
    </c:otherwise>
</c:choose>