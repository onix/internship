<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag trimDirectiveWhitespaces="true" %>

<p class="text-center">
    <a href="<c:url value="/"/>" class="btn btn-primary btn-large">
    <i class="icon-white icon-align-justify"></i> Go to main page</a>
</p>