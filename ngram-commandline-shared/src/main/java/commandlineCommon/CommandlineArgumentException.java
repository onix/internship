package commandlineCommon;

public class CommandlineArgumentException extends Exception {
    public CommandlineArgumentException(String message) {
        super(message);
    }

    public CommandlineArgumentException(String message, NumberFormatException cause) {
        super(message, cause);
    }
}
