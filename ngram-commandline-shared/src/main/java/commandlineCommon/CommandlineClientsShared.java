package commandlineCommon;

public class CommandlineClientsShared {
    private final static String help =
            "Lack of input parameters. Please, enter two of them in the following order:\n" +
                    "\tlength of n-gram as first parameter\n" +
                    "\ttext file name as a second parameter.\n\n" +
                    "For example,\n" +
                    "\tjava -jar ngram-statscollector 3 default.txt\n" +
                    "mean, words in default.txt will be splitted into 3-grams.";

    public static void checkInputParameters(String[] args) throws CommandlineArgumentException {
        final int ngramLength;
        final String fileName;

        if (args.length < 2) {
            System.out.println(help);
            System.exit(0);
        }

        try {
            ngramLength = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            throw new CommandlineArgumentException("Error in the first argument parsing: enter correct integer number.", e);
        }
        if (ngramLength < 1) {
            throw new CommandlineArgumentException("Enter valid ngram length (as '1' or more) as first parameter and file name as a second parameter!");
        }

        fileName = args[1];
        if (fileName.isEmpty()) {
            throw new CommandlineArgumentException("Enter file name as a second parameter!");
        }
    }
}
