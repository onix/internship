package logic;

import commandlineClient.NgramsCounter;
import libngram.PhraseDataHolder;
import libngram.WordWithNgrams;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NgramsCounterTest {
    private final static String sourcePhrase = "hello ollo lol armol io";

    private final static PhraseDataHolder sourcePhraseOnegram =
            new PhraseDataHolder(sourcePhrase,
                    new ArrayList<WordWithNgrams>() {{
                        add(
                                new WordWithNgrams(0, "hello",
                                        new ArrayList<String>() {{
                                            add("h");
                                            add("e");
                                            add("l");
                                            add("l");
                                            add("o");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(1, "ollo",
                                        new ArrayList<String>() {{
                                            add("o");
                                            add("l");
                                            add("l");
                                            add("o");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(2, "lol",
                                        new ArrayList<String>() {{
                                            add("l");
                                            add("o");
                                            add("l");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(3, "armol",
                                        new ArrayList<String>() {{
                                            add("a");
                                            add("r");
                                            add("m");
                                            add("o");
                                            add("l");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(4, "io",
                                        new ArrayList<String>() {{
                                            add("i");
                                            add("o");
                                        }}
                                )
                        );
                    }}
            );


    private final static Map<String, Integer> onegramCountedMap = new HashMap<String, Integer>() {{
        put("h", 1);
        put("e", 1);
        put("l", 7);
        put("o", 6);
        put("a", 1);
        put("r", 1);
        put("m", 1);
        put("i", 1);
    }};

    private final static PhraseDataHolder sourcePhraseTwogram =
            new PhraseDataHolder(sourcePhrase,
                    new ArrayList<WordWithNgrams>() {{
                        add(
                                new WordWithNgrams(0, "hello",
                                        new ArrayList<String>() {{
                                            add("he");
                                            add("el");
                                            add("ll");
                                            add("lo");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(1, "ollo",
                                        new ArrayList<String>() {{
                                            add("ol");
                                            add("ll");
                                            add("lo");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(2, "lol",
                                        new ArrayList<String>() {{
                                            add("lo");
                                            add("ol");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(3, "armol",
                                        new ArrayList<String>() {{
                                            add("ar");
                                            add("rm");
                                            add("mo");
                                            add("ol");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(4, "io",
                                        new ArrayList<String>() {{
                                            add("io");
                                        }}
                                )
                        );
                    }}
            );

    private final static Map<String, Integer> twogramCountedMap = new HashMap<String, Integer>() {{
        put("he", 1);
        put("el", 1);
        put("ll", 2);
        put("lo", 3);
        put("ol", 3);
        put("ar", 1);
        put("rm", 1);
        put("mo", 1);
        put("io", 1);
    }};

    private final static PhraseDataHolder sourcePhraseThreegram =
            new PhraseDataHolder(sourcePhrase,
                    new ArrayList<WordWithNgrams>() {{
                        add(
                                new WordWithNgrams(0, "hello",
                                        new ArrayList<String>() {{
                                            add("hel");
                                            add("ell");
                                            add("llo");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(1, "ollo",
                                        new ArrayList<String>() {{
                                            add("oll");
                                            add("llo");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(2, "lol",
                                        new ArrayList<String>() {{
                                            add("lol");
                                        }}
                                )
                        );
                        add(
                                new WordWithNgrams(3, "armol",
                                        new ArrayList<String>() {{
                                            add("arm");
                                            add("rmo");
                                            add("mol");
                                        }}
                                )
                        );
                        add(new WordWithNgrams(4, "io", null));
                    }}
            );

    private final static Map<String, Integer> threegramCountedMap = new HashMap<String, Integer>() {{
        put("hel", 1);
        put("ell", 1);
        put("llo", 2);
        put("oll", 1);
        put("lol", 1);
        put("arm", 1);
        put("rmo", 1);
        put("mol", 1);
    }};

    @DataProvider(name = "listMapDataVariable")
    public Object[][] createData() throws IOException {
        return new Object[][]{
                {sourcePhraseOnegram, onegramCountedMap},
                {sourcePhraseTwogram, twogramCountedMap},
                {sourcePhraseThreegram, threegramCountedMap}
        };
    }

    @Test(dataProvider = "listMapDataVariable")
    public void testCountNgramsToMap(PhraseDataHolder phraseWithNgramsModel, Map<String, Integer> resultMapModel) throws Exception {
        Map<String, Integer> mapToCompute = new HashMap<String, Integer>();
        NgramsCounter.countNgramsToMap(phraseWithNgramsModel, mapToCompute);

        Assert.assertEquals(mapToCompute, resultMapModel);
    }
}
