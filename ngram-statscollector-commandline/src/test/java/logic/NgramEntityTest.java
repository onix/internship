package logic;

import commandlineClient.NgramEntity;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class NgramEntityTest {
    private final String ngramOutputOne = "abc: 2";
    private final NgramEntity ngramOne = new NgramEntity("abc", 2);
    private final String ngramOutputTwo = "bbc: 6";
    private final NgramEntity ngramTwo = new NgramEntity("bbc", 6);
    private final String ngramOutputThree = "qwer: 11324";
    private final NgramEntity ngramThree = new NgramEntity("qwer", 11324);

    @DataProvider(name = "entityVariable")
    public Object[][] createData() throws IOException {
        return new Object[][]{
                {ngramOutputOne, ngramOne},
                {ngramOutputTwo, ngramTwo},
                {ngramOutputThree, ngramThree}
        };
    }

    @Test(dataProvider = "entityVariable")
    public void testNgramEntityToString(String expectedString, NgramEntity entity) throws Exception {
        Assert.assertEquals(expectedString, entity.toString());
    }
}