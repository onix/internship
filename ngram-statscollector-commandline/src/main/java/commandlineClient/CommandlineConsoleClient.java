package commandlineClient;

import commandlineCommon.CommandlineClientsShared;
import commandlineCommon.CommandlineArgumentException;
import commandlineClient.NgramEntity;
import commandlineClient.NgramsFromFileExtractor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class CommandlineConsoleClient {

    public static void main(String[] args) {
        try {
            CommandlineClientsShared.checkInputParameters(args);

            final int ngramLength = Integer.parseInt(args[0]);
            final String fileName = args[1];

            List<NgramEntity> listOfNgrams = NgramsFromFileExtractor.getSortedListFromMap(NgramsFromFileExtractor.extractSortedNgramsListFromFile(fileName, ngramLength));
            System.out.print(NgramsFromFileExtractor.ngramListToString(listOfNgrams));

        } catch (CommandlineArgumentException e) {
            System.out.println(e.getMessage());
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("Incorrect file data.");
        }
    }
}
