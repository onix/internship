package commandlineClient;

import commandlineClient.NgramEntity;
import commandlineClient.NgramsFromFileExtractor;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

public class InteractiveConsoleClient {

    public static void main(String[] args) throws IOException {
        String fileName;
        int ngramLength;
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));

        try {
            System.out.println("Enter file name ('default.txt' if empty): ");
            fileName = consoleReader.readLine();
            if (StringUtils.isEmpty(fileName)) {
                fileName = "default.txt";
            }

            System.out.println("Enter length of n-gram (3 if empty): ");
            String ngramStrLength = consoleReader.readLine();
            if (StringUtils.isEmpty(ngramStrLength)) {
                ngramLength = 3;
            } else {
                try {
                    ngramLength = Integer.parseInt(ngramStrLength);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Error in ngram length parsing: enter correct integer number.", e);
                }
                if (ngramLength < 1) {
                    throw new IllegalArgumentException("Enter valid ngram length (as '1' or more)!");
                }
            }

            List<NgramEntity> listOfNgrams = NgramsFromFileExtractor.getSortedListFromMap(NgramsFromFileExtractor.extractSortedNgramsListFromFile(fileName, ngramLength));
            System.out.print(NgramsFromFileExtractor.ngramListToString(listOfNgrams));

        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Incorrect file data or file not found.");
        } finally {
            consoleReader.close();
        }
    }
}
