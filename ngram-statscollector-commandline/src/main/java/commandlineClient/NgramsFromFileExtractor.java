package commandlineClient;

import libngram.PhrasesWithWordsAndNgramsFromFileExtractor;
import libngram.PhraseDataHolder;

import java.io.IOException;
import java.util.*;

import static org.apache.commons.lang3.StringUtils.isEmpty;

public class NgramsFromFileExtractor {

    public static Map<String, Integer> extractSortedNgramsListFromFile(String fileName, int ngramLength) throws IOException {
        if (isEmpty(fileName)) {
            throw new IllegalArgumentException("Method got empty file name argument.");
        }
        if (ngramLength < 1) {
            throw new IllegalArgumentException("Method got negative or zero ngram length argument.");
        }

        PhrasesWithWordsAndNgramsFromFileExtractor phrasesExtractor = null;
        try {
            phrasesExtractor = new PhrasesWithWordsAndNgramsFromFileExtractor(fileName, ngramLength);

            Map<String, Integer> ngrams = new HashMap<String, Integer>();

            for (PhraseDataHolder phraseWithNgrams : phrasesExtractor) {
                NgramsCounter.countNgramsToMap(phraseWithNgrams, ngrams);
            }

            return ngrams;
        } finally {
            if (phrasesExtractor != null) {
                phrasesExtractor.closeFile();
            }
        }
    }

    public static List<NgramEntity> getSortedListFromMap(Map<String, Integer> sourceMap) {
        if (sourceMap == null) {
            throw new IllegalArgumentException("Method got null source map argument.");
        }

        ArrayList<NgramEntity> sortedNgrams = new ArrayList<NgramEntity>(sourceMap.size());
        for (Map.Entry<String, Integer> entry : sourceMap.entrySet()) {
            sortedNgrams.add(new NgramEntity(entry.getKey(), entry.getValue()));
        }

        Collections.sort(sortedNgrams, Collections.reverseOrder(new NgramEntity.NgramEntityComparator()));
        return sortedNgrams;
    }

    public static String ngramListToString(List<NgramEntity> sortedNgrams) {
        if (sortedNgrams == null) {
            throw new IllegalArgumentException("Method got null list of ngrams argument.");
        }

        StringBuilder output = new StringBuilder();
        for (NgramEntity ngram : sortedNgrams) {
            output.append(ngram.toString()).append("\n");
        }
        return output.toString();
    }
}
