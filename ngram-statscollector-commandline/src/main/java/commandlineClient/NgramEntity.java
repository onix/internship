package commandlineClient;

import java.io.Serializable;
import java.util.Comparator;

public class NgramEntity {

    private final String ngram;
    private final int counter;

    public NgramEntity(String ngram, int counter) {
        if (ngram == null) {
            throw new IllegalArgumentException("Ngram object got its data field as null.");
        }
        if (counter < 1) {
            throw new IllegalArgumentException("Ngram object got its counter field as negative or zero.");
        }

        this.ngram = ngram;
        this.counter = counter;
    }

    @Override
    public String toString() {
        return ngram + ": " + counter;
    }

    public static class NgramEntityComparator implements Serializable, Comparator<NgramEntity> {
        private static final long serialVersionUID = 1L;

        @Override
        public int compare(NgramEntity o1, NgramEntity o2) {
            Integer counter1 = o1.counter;
            Integer counter2 = o2.counter;
            int counterCompare  = counter1.compareTo(counter2);

            if (counterCompare != 0) {
                return counterCompare;
            } else {
                String ngram1 = o1.ngram;
                String ngram2 = o2.ngram;
                return ngram2.compareTo(ngram1);
            }
        }
    }
}
