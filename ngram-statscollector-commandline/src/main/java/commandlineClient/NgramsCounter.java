package commandlineClient;

import libngram.PhraseDataHolder;
import libngram.WordWithNgrams;

import java.util.Map;

public class NgramsCounter {

    public static void countNgramsToMap(PhraseDataHolder phraseData, Map<String, Integer> mapToAdd) {
        if (phraseData == null) {
            throw new IllegalArgumentException("Method got null phrase data argument.");
        }
        if (mapToAdd == null) {
            throw new IllegalArgumentException("Method got null ngrams counter map argument.");
        }

        for (WordWithNgrams wordWithNgram : phraseData.getWordsWithNgramsList()) {
            if (wordWithNgram.getNgramsList() != null) {
                for (int i = 0; i < wordWithNgram.getNgramsList().size(); i++) {
                    addNgram(wordWithNgram.getNgramsList().get(i), mapToAdd);
                }
            }
        }
    }

    private static void addNgram(String ngram, Map<String, Integer> mapToAdd) {
        if (ngram == null) {
            throw new IllegalArgumentException("Method got null ngram data argument.");
        }
        if (mapToAdd == null) {
            throw new IllegalArgumentException("Method got null map to add argument.");
        }

        String inputInLowerCase = ngram.toLowerCase();
        if (mapToAdd.containsKey(inputInLowerCase)) {
            mapToAdd.put(inputInLowerCase, mapToAdd.get(inputInLowerCase) + 1);
        } else {
            mapToAdd.put(inputInLowerCase, 1);
        }
    }
}